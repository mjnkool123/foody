
import 'package:flutter/material.dart';
import 'package:foody/presentation/presentation.dart';

class Routes {
  static const String loginScreen = 'login_screen';
  static const String signupScreen = 'signup_screen';
  static const String otpScreen = 'otp_screen';
  static const String changePassScreen = 'change_pass_screen';
  static const String forgetPassScreen = 'forget_pass_screen';
  static const String roleScreen = 'role_screen';
  static const String adminHomeScreen = 'admin_home_screen';
  static const String reCeptionistScreen = 'receptionist_screen';

  static final Map<String, WidgetBuilder> routes = {
    loginScreen: (_) => LoginScreen(),
    signupScreen: (_) => RegisterScreen(),
    otpScreen: (_) => OtpReceiveScreen(),
    changePassScreen: (_) => ChangePasswordScreen(),
    forgetPassScreen: (_) => ForgotPasswordScreen(),
    roleScreen: (_) => RoleChooseScreen(),
    adminHomeScreen: (_) => AdminHomeScreen(),
    reCeptionistScreen: (_) => ReceptionistScreen(),
  };
}
