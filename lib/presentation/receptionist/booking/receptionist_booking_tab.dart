import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';
import 'package:foody/presentation/presentation.dart';
import 'package:md2_tab_indicator/md2_tab_indicator.dart';
import 'package:provider/provider.dart';

class ReceptionistBookingTab extends StatefulWidget {

  @override
  _ReceptionistBookingTabState createState() => _ReceptionistBookingTabState();
}

class _ReceptionistBookingTabState extends State<ReceptionistBookingTab> {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BookingViewModel>(context, listen: false);
    return PageView(
      physics: NeverScrollableScrollPhysics(),
      controller: provider.pageController,
      children: [
        BookingHome(provider: provider,),
        SetAppointment1(),
        SetAppointment2(),
        SetAppointment3(),
      ],
    );
  }

}

class BookingHome extends StatelessWidget {
  const BookingHome({
    Key key,
    this.provider,
  }) : super(key: key);

  final BookingViewModel provider;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Column(
        children: [
          Center(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ButtonTheme(
                child: RaisedButton(
                  onPressed: () => provider.nextPage(),
                  color: Colors.green,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  child: Container(
                    width: SizeConfig.blockSizeHorizontal * 60,
                    child: Center(
                      child: Text(
                        'TẠO LỊCH HẸN MỚI',
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            width: SizeConfig.blockSizeHorizontal * 68,
            child: Column(
              children: [
                Material(
                  elevation: 2.0,
                  shadowColor: Colors.grey,
                  borderRadius: BorderRadius.circular(25.7),
                  child: TextField(
                    autofocus: false,
                    style: TextStyle(fontSize: 17.0, color: Color(0xFFbdc6cf)),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: 'Nhập Mã Đặt Chỗ',
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(25.7),
                      ),
                      suffixIcon: InkWell(
                        child: Icon(Icons.search),
                        onTap: () {},
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Colors.white),
                        borderRadius: BorderRadius.circular(25.7),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                CustomDropdown(
                  shadow: true,
                  onChanged: (value) {},
                  items: [
                    DropdownMenuItem(
                      child: Text('Khu 1'),
                    ),
                    DropdownMenuItem(
                      child: Text('Khu 2'),
                    ),
                    DropdownMenuItem(
                      child: Text('Khu 3'),
                    ),
                  ],
                  hint: 'Tất Cả Khu',
                ),
                CustomDropdown(
                  shadow: true,
                  onChanged: (value) {},
                  items: [
                    DropdownMenuItem(
                      child: Text('Tầng 1'),
                    ),
                    DropdownMenuItem(
                      child: Text('Tầng 2'),
                    ),
                    DropdownMenuItem(
                      child: Text('Tầng 3'),
                    ),
                  ],
                  hint: 'Tầng 1-3',
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Expanded(
              child: Checking()
          ),
        ],
      ),
    );
  }
}


class Checking extends StatelessWidget {
  const Checking({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Column(
          children: [
            TabBar(
              indicator: MD2Indicator(
                indicatorSize: MD2IndicatorSize.normal,
                indicatorHeight: 5,
                indicatorColor: Colors.red,
              ),
              labelPadding: EdgeInsets.all(2.0),
              indicatorWeight: 2,
              indicatorSize: TabBarIndicatorSize.label,
              labelColor: Colors.red,
              unselectedLabelColor: Colors.grey,
              tabs: [
                Tab(
                    child: Text("TÌNH TRẠNG BOOKING",
                        style: TextStyle(fontWeight: FontWeight.bold))),
                Tab(
                    child: Text("LỊCH HẸN",
                        style: TextStyle(fontWeight: FontWeight.bold))),
              ],
            ),
            Expanded(
                child: TabBarView(
              children: [
                BookingStatusTab(),
                AppointmentTab(),
              ],
            ))
          ],
        ));
  }
}
