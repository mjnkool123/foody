import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';

import '../../../presentation.dart';

class AppointmentTab extends StatefulWidget {
  @override
  _AppointmentTabState createState() => _AppointmentTabState();
}

class _AppointmentTabState extends State<AppointmentTab> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            DotWidget(
              color: Colors.greenAccent,
              text: "Đã Tới Giờ Hẹn",
            ),
            DotWidget(
              color: Colors.yellow[600],
              text: "Sắp Tới",
            ),
            DotWidget(
              color: Colors.red,
              text: "Đã Hủy",
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Expanded(
          child: Stack(
            alignment: Alignment.center,
            children: [
              Align(
                alignment: Alignment.bottomCenter,
                child: Image.asset('assets/images/login_bg.png'),
              ),
              Container(
                width: SizeConfig.safeBlockHorizontal * 90,
                padding: EdgeInsets.only(
                  top: 10,
                  right: 20,
                  left: 20,
                  bottom: 10,
                ),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.8),
                ),
                child: ListView(
                  children: <Widget>[
                    ContactInfo(
                      name: "KEM",
                      phoneNumber: "0796664119",
                      code: "090998",
                      setTime: "9:00",
                      status: ContactInfoStatus.CAME,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ContactInfo(
                      name: "Hòa",
                      phoneNumber: "0796664119",
                      code: "090998",
                      setTime: "10:00",
                      status: ContactInfoStatus.COMING,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ContactInfo(
                      name: "FI ĐẠI",
                      phoneNumber: "0796664119",
                      code: "090998",
                      setTime: "11:00",
                      status: ContactInfoStatus.CANCEL,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ContactInfo(
                      name: "BẰNG",
                      phoneNumber: "0796664119",
                      code: "090998",
                      setTime: "12:00",
                      status: ContactInfoStatus.COMING,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ContactInfo(
                      name: "CƯỜNG",
                      phoneNumber: "0796664119",
                      code: "090998",
                      setTime: "1:00",
                      status: ContactInfoStatus.CANCEL,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
