import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';

import '../../../presentation.dart';

class BookingStatusTab extends StatefulWidget {
  @override
  _BookingStatusTabSate createState() => _BookingStatusTabSate();
}

class _BookingStatusTabSate extends State<BookingStatusTab> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: [
        SizedBox(
          height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            DotWidget(
              color: Colors.greenAccent,
              text: "Còn Trống",
            ),
            DotWidget(
              color: Colors.yellow[600],
              text: "Đã Đặt",
            ),
            DotWidget(
              color: Colors.red,
              text: "Khách Đã Tới",
            ),
          ],
        ),
        SizedBox(
          height: 20,
        ),
        Expanded(
          child: Container(
            color: Colors.black87,
            child: GridView(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
            children: [
              TableWidget(chairsSlot: 4, status: TableStatus.BOOKED, tableName: "Bàn 1",),
              TableWidget(chairsSlot: 4, status: TableStatus.BOOKED, tableName: "Bàn 2",),
              TableWidget(chairsSlot: 4, status: TableStatus.EMPTY, tableName: "Bàn 3",),
              TableWidget(chairsSlot: 6, status: TableStatus.EMPTY,tableName: "Bàn 4", booked: 2,),
              TableWidget(chairsSlot: 10, status: TableStatus.EMPTY, tableName: "Bàn 5",),
              TableWidget(chairsSlot: 7, status: TableStatus.CAME, tableName: "Bàn 6",),
              TableWidget(chairsSlot: 8, status: TableStatus.EMPTY, tableName: "Bàn 7",),
              TableWidget(chairsSlot: 3, status: TableStatus.EMPTY, tableName: "Bàn 8", booked: 1,),
              TableWidget(chairsSlot: 5, status: TableStatus.EMPTY, tableName: "Bàn 9", booked: 3,),
            ],
        ),
          ),)
      ],
    );
  }
}