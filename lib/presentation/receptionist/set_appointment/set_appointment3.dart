import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';
import 'package:foody/presentation/presentation.dart';
import 'package:provider/provider.dart';

class SetAppointment3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BookingViewModel>(context, listen: false);
    return Column(
      children: [
        SizedBox(height: SizeConfig.safeBlockVertical * 5,),
        Text(
          "LỊCH HẸN ĐÃ ĐẶT",
          style: TextStyle(
              fontSize: SizeConfig.safeBlockHorizontal * 6,
              fontWeight: FontWeight.bold),
        ),
        SizedBox(height: 20,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                Text(
                  "15/05",
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                Text(
                  "Ngày Hẹn",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                )
              ],
            ),
            Column(
              children: [
                Text(
                  "09:00",
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                Text(
                  "Giờ Hẹn",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                )
              ],
            ),
            Column(
              children: [
                Text(
                  "10",
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                Text(
                  "Chỗ",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                )
              ],
            )
          ],
        ),
        SizedBox(height: 10,),
        Text("______________________________________"),
        SizedBox(height: 10,),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                Text(
                  "1-3",
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                Text(
                  "Bàn Số",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                )
              ],
            ),
            Column(
              children: [
                Text(
                  "1",
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                Text(
                  "Khu Vục",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                )
              ],
            ),
            Column(
              children: [
                Text(
                  "3",
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                Text(
                  "Tầng",
                  style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                )
              ],
            )
          ],
        ),
        SizedBox(height: 10,),
        Expanded(
            child: Container(
              padding: EdgeInsets.only(
                  right: SizeConfig.safeBlockHorizontal * 4,
                  left: SizeConfig.safeBlockHorizontal * 4,
              ),
              color: Colors.black87,
              child: Column(
                children: [
                  SizedBox(height: 5,),
                  Text(
                    "THÔNG TIN KHÁCH HÀNG",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: SizeConfig.safeBlockHorizontal * 6),
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Text(
                            "Kem",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 24),
                          ),
                          Text(
                            "Họ Và Tên",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14),
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            "090998",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 24),
                          ),
                          Text(
                            "Mã Đặt Chỗ",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14),
                          )
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "0796664119",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 24),
                          ),
                          Text(
                            "Số Điện Thoại",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14),
                          )
                        ],
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            "10",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 24),
                          ),
                          Text(
                            "Số Lượng Khách Sẽ Đến",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14),
                          )
                        ],
                      )
                    ],
                  ),
                  SizedBox(height: 10,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Text(
                            "09/09",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 24),
                          ),
                          Text(
                            "Sinh Nhật",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14),
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Text(
                            "Đã Gửi",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 24),
                          ),
                          Text(
                            "SMS",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14),
                          )
                        ],
                      ),
                    ],
                  ),
                  SizedBox(height: 20,),
                  ButtonTheme(
                    child: RaisedButton(
                      onPressed: () {
                        provider.backToHome();
                      },
                      color: Colors.green,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      child: Container(
                        width: SizeConfig.blockSizeHorizontal * 70,
                        child: Center(
                          child: Text(
                            'Quay Lại Màn Hình Chính',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )),
      ],
    );
  }
}
