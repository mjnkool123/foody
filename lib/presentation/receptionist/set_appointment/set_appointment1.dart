import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';
import 'package:foody/presentation/presentation.dart';
import 'package:provider/provider.dart';

class SetAppointment1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BookingViewModel>(context, listen: false);
    return Center(
      child: SetAppointment1Home(provider: provider,),
    );
  }

}

class SetAppointment1Home extends StatelessWidget {
  const SetAppointment1Home({
    Key key,
    this.provider,
  }) : super(key: key);

  final BookingViewModel provider;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Align(
          alignment: Alignment.bottomCenter,
          child: Image.asset('assets/images/login_bg.png'),),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text("CHỌN NGÀY", style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 6),),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: CustomSelect(
                buttons: _listDate(),
                onChanged: (value) {},
                defaultSelect: 0,
                paddingContent: EdgeInsets.symmetric(
                  horizontal: 10,
                  vertical: 15,
                ),
                space: 10,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.black12, width: 1),
                ),
                selectDecoration: BoxDecoration(
                  color: Colors.greenAccent,
                  borderRadius: BorderRadius.circular(10),
                ),
                textStyle: TextStyle(
                  color: Colors.black87,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
                selectStyle: TextStyle(
                    color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
            Text("CHỌN GIỜ", style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 6),),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: CustomSelect(
                buttons: [
                  CustomSelectButton(
                    child: Text('08:00'),
                    value: 8,
                  ),
                  CustomSelectButton(
                    child: Text('09:00'),
                    value: 9,
                  ),
                  CustomSelectButton(
                    child: Text('10:00'),
                    value: 10,
                  ),
                  CustomSelectButton(
                    child: Text('11:00'),
                    value: 11,
                  ),
                ],
                onChanged: (value) {},
                defaultSelect: 0,
                paddingContent: EdgeInsets.symmetric(
                  horizontal: 12,
                  vertical: 12,
                ),
                space: 10,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30),
                  border: Border.all(color: Colors.black12, width: 1),
                ),
                selectDecoration: BoxDecoration(
                  color: Colors.yellow[600],
                  borderRadius: BorderRadius.circular(30),
                ),
                textStyle: TextStyle(
                  color: Colors.black87,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
                selectStyle: TextStyle(
                    color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),
              ),
            ),
            Text("CHỌN VỊ TRÍ", style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 6),),
            Container(
              width: SizeConfig.blockSizeHorizontal * 85,
              child: Column(
                children: [
                  Material(
                    elevation: 2.0,
                    shadowColor: Colors.grey,
                    borderRadius: BorderRadius.circular(25.7),
                    child: TextField(
                      autofocus: false,
                      style: TextStyle(fontSize: 17.0, color: Color(0xFFbdc6cf)),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Số Lượng Khách',
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  CustomDropdown(
                    shadow: true,
                    onChanged: (value) {},
                    items: [
                      DropdownMenuItem(
                        child: Text('Khu 1'),
                      ),
                      DropdownMenuItem(
                        child: Text('Khu 2'),
                      ),
                      DropdownMenuItem(
                        child: Text('Khu 3'),
                      ),
                    ],
                    hint: 'Tất Cả Khu',
                  ),
                  CustomDropdown(
                    shadow: true,
                    onChanged: (value) {},
                    items: [
                      DropdownMenuItem(
                        child: Text('Tầng 1'),
                      ),
                      DropdownMenuItem(
                        child: Text('Tầng 2'),
                      ),
                      DropdownMenuItem(
                        child: Text('Tầng 3'),
                      ),
                    ],
                    hint: 'Tầng 1-3',
                  ),
                ],
              ),
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ButtonTheme(
                  child: RaisedButton(
                    onPressed: () => provider.nextPage(),
                    color: Colors.green,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Container(
                      width: SizeConfig.blockSizeHorizontal * 60,
                      child: Center(
                        child: Text(
                          'TIẾP THEO',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  _listDate() {
    final buttons = <CustomSelectButton>[];
    var now = DateTime.now();
    for (var i = 0; i < 7; i++) {
      final current = now.add(Duration(days: i));
      buttons.add(
        CustomSelectButton(
          child: Column(
            children: [
              Text('${current.day}'),
              SizedBox(height: 6),
              Text(
                now.day == current.day ? 'Hôm Nay' : 'Thứ ${current.weekday}',
                style: TextStyle(fontWeight: FontWeight.w400),
              ),
            ],
          ),
          value: i,
        ),
      );
    }
    return buttons;
  }
}