import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';
import 'package:provider/provider.dart';

import '../../presentation.dart';

class SetAppointment2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: SetAppointment2Home(),
    );
  }
}

class SetAppointment2Home extends StatelessWidget {
  const SetAppointment2Home({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '15/05',
                  style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 5, fontWeight: FontWeight.bold),
                ),
                Text('Ngày hẹn'),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '09:00',
                  style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 5, fontWeight: FontWeight.bold),
                ),
                Text('Giờ hẹn'),
              ],
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '10',
                  style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 5, fontWeight: FontWeight.bold),
                ),
                Text('Chỗ'),
              ],
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Text("CHỌN BÀN", style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 6),),
        SizedBox(
          height: 10,
        ),
        Container(
          width: SizeConfig.blockSizeHorizontal * 85,
          child: Column(
            children: [
              CustomDropdown(
                shadow: true,
                onChanged: (value) {},
                items: [
                  DropdownMenuItem(
                    child: Text('Khu 1'),
                  ),
                  DropdownMenuItem(
                    child: Text('Khu 2'),
                  ),
                  DropdownMenuItem(
                    child: Text('Khu 3'),
                  ),
                ],
                hint: 'Tất Cả Khu',
              ),
              CustomDropdown(
                shadow: true,
                onChanged: (value) {},
                items: [
                  DropdownMenuItem(
                    child: Text('Tầng 1'),
                  ),
                  DropdownMenuItem(
                    child: Text('Tầng 2'),
                  ),
                  DropdownMenuItem(
                    child: Text('Tầng 3'),
                  ),
                ],
                hint: 'Tầng 1-3',
              ),
            ],
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            DotWidget(
              color: Colors.greenAccent,
              text: "Còn Trống",
            ),
            DotWidget(
              color: Colors.yellow[600],
              text: "Đã Đặt",
            ),
            DotWidget(
              color: Colors.red,
              text: "Khách Đã Tới",
            ),
          ],
        ),
        TableMultiChoice()
      ],
    );
  }
}

class TableMultiChoice extends StatelessWidget {
  const TableMultiChoice({
    Key key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<BookingViewModel>(context, listen: false);
    bool _lights = true;
    return Expanded(
      child: Container(
        color: Colors.black87,
        child: Column(
          children: [
            SizedBox(height: 10),
            Text(
              'Khu 1 - Tầng 3',
              style: TextStyle(color: Colors.white, fontSize: SizeConfig.safeBlockHorizontal * 6),
            ),
            Expanded(
              child: TableMultiSelect(
                buttons: [
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 4,
                      status: TableStatus.BOOKED,
                      tableName: "Bàn 1",
                      booked: 4,
                      showText: true,
                    ),
                    value: 'table0',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 4,
                      status: TableStatus.BOOKED,
                      tableName: "Bàn 3",
                      booked: 4,
                      showText: true,
                    ),
                    value: 'table1',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 4,
                      status: TableStatus.EMPTY,
                      tableName: "Bàn 3",
                      booked: 2,
                      showText: true,
                    ),
                    value: 'table2',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 6,
                      status: TableStatus.EMPTY,
                      booked: 2,
                      showText: true,
                      tableName: "Bàn 4",
                    ),
                    value: 'table3',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 10,
                      status: TableStatus.BOOKED,
                      booked: 4,
                      tableName: "Bàn 5",
                      showText: true,
                    ),
                    value: 'table4',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 2,
                      status: TableStatus.CAME,
                      tableName: "Bàn 6",
                    ),
                    value: 'table0',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 2,
                      status: TableStatus.CAME,
                      tableName: "Bàn 7",
                    ),
                    value: 'table0',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 4,
                      status: TableStatus.BOOKED,
                      tableName: "Bàn 8",
                      booked: 4,
                      showText: true,
                    ),
                    value: 'table0',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 4,
                      status: TableStatus.BOOKED,
                      tableName: "Bàn 9",
                      booked: 4,
                      showText: true,
                    ),
                    value: 'table1',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 4,
                      status: TableStatus.EMPTY,
                      tableName: "Bàn 10",
                      booked: 2,
                      showText: true,
                    ),
                    value: 'table2',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 6,
                      status: TableStatus.EMPTY,
                      booked: 2,
                      showText: true,
                      tableName: "Bàn 11",
                    ),
                    value: 'table3',
                  ),
                  CustomMultiSelectButton(
                    child: TableWidget(
                      chairsSlot: 2,
                      status: TableStatus.CAME,
                      tableName: "Bàn 12",
                    ),
                    value: 'table0',
                  ),
                ],
                onChanged: (value) {
                  print(value);
                },
                onLongTap: (value) {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return ChairSelect();
                      });
                },
                selectDecoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text("Chọn Cả Khu", style: TextStyle(color: Colors.white),),
                CupertinoSwitch(value: _lights,   onChanged: (bool value) {})
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text("Chọn Cả Tầng", style: TextStyle(color: Colors.white),),
                CupertinoSwitch(value: _lights,   onChanged: (bool value) {})
              ],
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ButtonTheme(
                  child: RaisedButton(
                    onPressed: () {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return UserInfo(provider: provider,);
                          });
                    },
                    color: Colors.green,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Container(
                      width: SizeConfig.blockSizeHorizontal * 60,
                      child: Center(
                        child: Text(
                          'NHẬP THÔNG TIN',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ChairSelect extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Scaffold(
        backgroundColor: Colors.black54,
        appBar: AppBar(
          leading: IconButton(
            onPressed: (){
              Navigator.pop(context);
            },
            icon: Icon(Icons.close, size: 30,),
          ),
          backgroundColor: Colors.black54,
        ),
        body: Container(
          padding: EdgeInsets.only(right: 8.0, left: 8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Chọn ghế", style: TextStyle(color: Colors.white, fontSize: 30, fontWeight: FontWeight.bold),),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      Text("10", style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),),
                      Text("Số Ghế Khách Đặt", style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),)
                    ],
                  ),
                  Column(
                    children: [
                      Text("8", style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),),
                      Text("Số Ghế Đã Chọn", style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold),)
                    ],
                  )
                ],
              ),
              ChairPick(
                chairsSlot: 4,
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ButtonTheme(
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      color: Colors.yellow[600],
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      child: Container(
                        width: SizeConfig.blockSizeHorizontal * 60,
                        child: Center(
                          child: Text(
                            'XONG',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class UserInfo extends StatelessWidget {
  final BookingViewModel provider;
  const UserInfo({Key key, this.provider}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    bool _lights = true;
    return Material(
      color: Colors.transparent,
      child: Scaffold(
        backgroundColor: Colors.black54,
        appBar: AppBar(
          leading: IconButton(
            onPressed: (){
              Navigator.pop(context);
            },
            icon: Icon(Icons.close, size: 30,),
          ),
          backgroundColor: Colors.black54,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("THÔNG TIN KHÁCH HÀNG", style: TextStyle(color: Colors.white, fontSize: SizeConfig.safeBlockHorizontal * 6, fontWeight: FontWeight.bold),),
              SizedBox(height: 20,),
              Container(
                width: SizeConfig.blockSizeHorizontal * 85,
                child: Column(
                  children: [
                    Material(
                      elevation: 2.0,
                      shadowColor: Colors.grey,
                      borderRadius: BorderRadius.circular(25.7),
                      child: TextField(
                        autofocus: false,
                        style: TextStyle(fontSize: 17.0, color: Color(0xFFbdc6cf)),
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: 'Họ Và Tên',
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Material(
                      elevation: 2.0,
                      shadowColor: Colors.grey,
                      borderRadius: BorderRadius.circular(25.7),
                      child: TextField(
                        autofocus: false,
                        style: TextStyle(fontSize: 17.0, color: Color(0xFFbdc6cf)),
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: 'Sinh Nhật',
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                          suffixIcon: Icon(Icons.calendar_today_outlined),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Material(
                      elevation: 2.0,
                      shadowColor: Colors.grey,
                      borderRadius: BorderRadius.circular(25.7),
                      child: TextField(
                        autofocus: false,
                        style: TextStyle(fontSize: 17.0, color: Color(0xFFbdc6cf)),
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: 'Số Điện Thoại',
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Material(
                      elevation: 2.0,
                      shadowColor: Colors.grey,
                      borderRadius: BorderRadius.circular(25.7),
                      child: TextField(
                        autofocus: false,
                        style: TextStyle(fontSize: 17.0, color: Color(0xFFbdc6cf)),
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          hintText: 'Số Lượng Khách Sẽ Đến',
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 10,),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: Material(
                          elevation: 2.0,
                          shadowColor: Colors.grey,
                          borderRadius: BorderRadius.circular(25.7),
                          child: TextField(
                            autofocus: false,
                            style: TextStyle(fontSize: 17.0, color: Color(0xFFbdc6cf)),
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white,
                              hintText: 'Số Lượng Nam',
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(25.7),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.white),
                                borderRadius: BorderRadius.circular(25.7),
                              ),
                            ),
                          ),
                        ),),
                        SizedBox(width: 10,),
                        Flexible(
                          flex: 1,
                          child: Material(
                            elevation: 2.0,
                            shadowColor: Colors.grey,
                            borderRadius: BorderRadius.circular(25.7),
                            child: TextField(
                              autofocus: false,
                              style: TextStyle(fontSize: 17.0, color: Color(0xFFbdc6cf)),
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white,
                                hintText: 'Số Lượng Nữ',
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(25.7),
                                ),
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white),
                                  borderRadius: BorderRadius.circular(25.7),
                                ),
                              ),
                            ),
                          ),),
                      ],
                    )
                  ],
                ),
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text("Gửi SMS Thông Báo", style: TextStyle(color: Colors.white),),
                  CupertinoSwitch(value: _lights,   onChanged: (bool value) {})
                ],
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ButtonTheme(
                    child: RaisedButton(
                      onPressed: () => {
                      Navigator.pop(context),
                        provider.nextPage()
                      },
                      color: Colors.green,
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(30.0)),
                      child: Container(
                        width: SizeConfig.blockSizeHorizontal * 70,
                        child: Center(
                          child: Text(
                            'XONG',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}