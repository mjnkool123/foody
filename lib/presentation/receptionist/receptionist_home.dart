import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/presentation/presentation.dart';
import 'package:md2_tab_indicator/md2_tab_indicator.dart';
import 'package:provider/provider.dart';

class ReceptionistScreen extends StatefulWidget {
  @override
  _ReceptionistScreenState createState() => _ReceptionistScreenState();
}

class _ReceptionistScreenState extends State<ReceptionistScreen>
    with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: 4, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => _checkPop(),
      child: DefaultTabController(
          length: 4,
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            body: NestedScrollView(
              headerSliverBuilder:
                  (BuildContext context, bool innerBoxIsScrolled) {
                return <Widget>[
                  SliverAppBar(
                    backgroundColor: Colors.green,
                    floating: true,
                    snap: true,
                    bottom: PreferredSize(
                      preferredSize: const Size.fromHeight(100),
                      child: Stack(
                        children: <Widget>[
                          Container(
                            color: Colors.black87,
                            height: 150,
                            child: Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                margin: EdgeInsets.only(bottom: 15),
                                child: TabBar(
                                  controller: _tabController,
                                  indicator: MD2Indicator(
                                    indicatorSize: MD2IndicatorSize.normal,
                                    indicatorHeight: 5,
                                    indicatorColor: Colors.yellow,
                                  ),
                                  labelPadding: EdgeInsets.all(2.0),
                                  indicatorWeight: 2,
                                  indicatorSize: TabBarIndicatorSize.label,
                                  labelColor: Colors.yellow[400],
                                  unselectedLabelColor: Colors.grey,
                                  tabs: [
                                    Tab(
                                        child: Text("ĐẶT BÀN",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold))),
                                    Tab(
                                        child: Text("GỌI MÓN",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold))),
                                    Tab(
                                        child: Text("PHỤC VỤ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold))),
                                    Tab(
                                        child: Text("THANH TOÁN",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold))),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Container(
                            color: Colors.green,
                            height: 50,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: IconButton(
                                      icon: Icon(
                                        Icons.arrow_back_ios,
                                        color: Colors.white,
                                      ),
                                      onPressed: null),
                                ),
                                IconButton(
                                    icon: Icon(
                                      Icons.menu,
                                      color: Colors.white,
                                      size: 34,
                                    ),
                                    onPressed: null),
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            heightFactor: 1.0,
                            child: Card(
                              color: Colors.transparent,
                              elevation: 0.0,
                              child: Container(
                                alignment: Alignment.bottomCenter,
                                width: 80.0,
                                height: 80.0,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/images/avatar_letan.jpeg'),
                                        fit: BoxFit.fill),
                                    border: Border.all(
                                        width: 4.0, color: Colors.white)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ];
              },
              body: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  ReceptionistBookingTab(),
                  Center(
                    child: Text("null"),
                  ),
                  Center(
                    child: Text("null"),
                  ),
                  Center(
                    child: Text("null"),
                  ),
                ],
              ),
            ),
          )),
    );
  }

  _checkPop() {
    if (_tabController.index == 0) {
      return Provider.of<BookingViewModel>(context, listen: false).checkPop();
    } else {
      setState(() {
        _tabController.index--;
      });
      return false;
    }
  }
}
