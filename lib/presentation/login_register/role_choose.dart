import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';
import 'package:foody/utils/routes.dart';
import '../presentation.dart';

class RoleChooseScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      backgroundColor: Colors.green,
      resizeToAvoidBottomPadding: false,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "CHỌN VAI TRÒ",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: SizeConfig.safeBlockHorizontal * 6),
            ),
            SizedBox(
              height: 40,
            ),
            InkWell(
              onTap: () {
                Navigator.pushReplacementNamed(
                    context, Routes.reCeptionistScreen);
              },
              child: Card(
                color: Colors.transparent,
                elevation: 0.0,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  width: 80.0,
                  height: 80.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage(
                              'assets/images/avatar_letan.jpeg'),
                          fit: BoxFit.fill),
                      border: Border.all(
                          width: 4.0, color: Colors.white)),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "LỄ TÂN",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: SizeConfig.safeBlockHorizontal * 5),
            ),
            SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () {},
              child: Card(
                color: Colors.transparent,
                elevation: 0.0,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  width: 80.0,
                  height: 80.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage(
                              'assets/images/avatar_chef.jpeg'),
                          fit: BoxFit.fill),
                      border: Border.all(
                          width: 4.0, color: Colors.white)),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "ĐẦU BẾP",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: SizeConfig.safeBlockHorizontal * 5),
            ),
            SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () {
                Navigator.pushReplacementNamed(
                    context, Routes.adminHomeScreen);
              },
              child: Card(
                color: Colors.transparent,
                elevation: 0.0,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  width: 80.0,
                  height: 80.0,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          image: AssetImage(
                              'assets/images/avatar_admin.jpeg'),
                          fit: BoxFit.fill),
                      border: Border.all(
                          width: 4.0, color: Colors.white)),
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "ADMIN",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: SizeConfig.safeBlockHorizontal * 5),
            ),
          ],
        ),
      ),
    );
  }
}
