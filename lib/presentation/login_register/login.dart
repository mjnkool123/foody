import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';

import '../presentation.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(SizeConfig.safeBlockVertical * 20),
        child: AppBar(
          backgroundColor: Colors.green,
          automaticallyImplyLeading: false,
          flexibleSpace: Image.asset('assets/images/logo.png'),
        ),
      ),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Image.asset('assets/images/login_bg.png'),
          ),
          FormLogin(),
          AnotherChoice(),
        ],
      ),
    );
  }
}

class FormLogin extends StatelessWidget {
  const FormLogin({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Stack(
        alignment: Alignment.topCenter,
        overflow: Overflow.visible,
        children: [
          Container(
            height: SizeConfig.safeBlockVertical * 45,
            width: SizeConfig.safeBlockHorizontal * 90,
            padding: EdgeInsets.only(
              top: 10,
              right: 20,
              left: 20,
              bottom: 10,
            ),
            decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(20)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("ĐĂNG NHẬP", style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 7,),),
                SizedBox(height: 10,),
                Material(
                  elevation: 3.0,
                  shadowColor: Colors.grey,
                  borderRadius: BorderRadius.circular(25.7),
                  child: TextField(
                    autofocus: false,
                    style: TextStyle(
                        fontSize: 17.0, color: Color(0xFFbdc6cf)),
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: 'Tài Khoản/Số Điện Thoại',
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.white),
                        borderRadius: BorderRadius.circular(25.7),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.white),
                        borderRadius: BorderRadius.circular(25.7),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10,),
                Material(
                  elevation: 3.0,
                  shadowColor: Colors.blue,
                  borderRadius: BorderRadius.circular(25.7),
                  child: TextField(
                    autofocus: false,
                    style: TextStyle(
                        fontSize: 17.0, color: Color(0xFFbdc6cf)),
                    decoration: InputDecoration(
                      suffixIcon: Icon(
                          Icons.remove_red_eye_sharp),
                      filled: true,
                      fillColor: Colors.white,
                      hintText: 'Mật Khẩu',
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.white),
                        borderRadius: BorderRadius.circular(25.7),
                      ),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(
                            color: Colors.white),
                        borderRadius: BorderRadius.circular(25.7),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Row(
                      children: [
                        Checkbox(
                          activeColor: Colors.green,
                          value: true,
                          onChanged: (value) {},
                        ),
                        Text("Lưu Đăng Nhập")
                      ],
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) =>
                              ForgotPasswordScreen()),
                        );
                      },
                      child: Text("Quên mật khẩu?",
                        style: TextStyle(
                            decoration: TextDecoration.underline),
                      ),
                    ),
                  ],
                ),
                ButtonTheme(
                  minWidth: SizeConfig.safeBlockHorizontal * 90,
                  child: RaisedButton(onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(
                            builder: (context) => RoleChooseScreen())
                    );
                  },
                    color: Colors.green,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Text("Đăng Nhập",
                        style: new TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold)
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            bottom: -60,
            child: IconButton(
              iconSize: SizeConfig.safeBlockHorizontal * 20,
              onPressed: (){},
              icon: Image.asset(
                'assets/images/btn_fingerprint.png',
                height: 80,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class AnotherChoice extends StatelessWidget {
  const AnotherChoice({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: SizeConfig.safeBlockVertical * 20,
        width: SizeConfig.safeBlockHorizontal * 90,
        padding: EdgeInsets.only(
          top: 10,
          right: 20,
          left: 20,
          bottom: 10,
        ),
        decoration: BoxDecoration(
          color: Colors.grey[200].withOpacity(0.8),
          borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
        ),
        child: Column(
          children: [
            Text(
              "Đăng Nhập Bằng Tài Khoản Liên Kết",
              style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 4,),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  iconSize: SizeConfig.safeBlockHorizontal * 10,
                  onPressed: (){},
                  icon: Image.asset(
                    'assets/images/icon_fb.png',
                  ),
                ),
                IconButton(
                  iconSize: SizeConfig.safeBlockHorizontal * 10,
                  onPressed: (){},
                  icon: Image.asset(
                    'assets/images/icon_zalo.png',
                  ),
                ),
                IconButton(
                  iconSize: SizeConfig.safeBlockHorizontal * 10,
                  onPressed: (){},
                  icon: Image.asset(
                    'assets/images/icon_google.png',
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text("Chưa Có Tài Khoản?",
                  style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 4,),),
                InkWell(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) =>
                          RegisterScreen()),
                    );
                  },
                  child: Text("Đăng Ký Ngay",
                    style: TextStyle(
                        fontSize: SizeConfig.safeBlockHorizontal * 4,
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}