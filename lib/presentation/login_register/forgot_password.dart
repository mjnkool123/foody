import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';
import '../presentation.dart';

class ForgotPasswordScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(SizeConfig.safeBlockVertical * 20),
        child: AppBar(
          backgroundColor: Colors.green,
          automaticallyImplyLeading: false,
          flexibleSpace: Image.asset('assets/images/logo.png'),
        ),
      ),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Image.asset('assets/images/login_bg.png'),
          ),
          GetPassword(),
        ],
      ),
    );
  }
}

class GetPassword extends StatelessWidget {
  const GetPassword({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        height: SizeConfig.safeBlockVertical * 55,
        width: SizeConfig.safeBlockHorizontal * 90,
        padding: EdgeInsets.only(
          top: 10,
          right: 20,
          left: 20,
          bottom: 10,
        ),
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.vertical(bottom: Radius.circular(20)),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text("LẤY LẠI MẬT KHẨU", style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 7,),),
            Text("TÀI KHOẢN QUA SỐ ĐIỆN THOẠI", style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 5,),),
            SizedBox(height: 10,),
            Text("Chúng Tôi Sẽ Gửi Mã Xác Nhận OTP Đến Số Điện Thoại Của Bạn Trong Ít Phút",textAlign: TextAlign.center ,style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 4,),),
            SizedBox(height: 20,),
            CustomDropdown(
              shadow: true,
              onChanged: (value) {},
              items: [
                DropdownMenuItem(
                  child: Text('Việt Nam (+84)'),
                ),
                DropdownMenuItem(
                  child: Text('Lào (+85)'),
                ),
                DropdownMenuItem(
                  child: Text('Thái Lan (+80)'),
                ),
              ],
              hint: 'Việt Nam (+84)',
            ),
            Material(
              elevation: 3.0,
              shadowColor: Colors.grey[100],
              borderRadius: BorderRadius.circular(25.7),
              child: TextField(
                autofocus: false,
                style: TextStyle(
                    fontSize: 17.0, color: Color(0xFFbdc6cf)),
                decoration: InputDecoration(
                  suffixIcon: Icon(
                      Icons.check_circle, color: Colors.green),
                  filled: true,
                  fillColor: Colors.white,
                  hintText: 'Việt Nam (+84)',
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.white),
                    borderRadius: BorderRadius.circular(25.7),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                        color: Colors.white),
                    borderRadius: BorderRadius.circular(25.7),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20,),
            ButtonTheme(
              minWidth: SizeConfig.safeBlockHorizontal * 90,
              child: RaisedButton(onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(
                        builder: (context) => ChangePasswordScreen())
                );
              },
                color: Colors.green,
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(30.0)),
                child: Text("Tiếp Theo",
                    style: new TextStyle(
                        color: Colors.white,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold)
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
