import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';
import '../presentation.dart';

class OtpReceiveScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(SizeConfig.safeBlockVertical * 20),
        child: AppBar(
          backgroundColor: Colors.green,
          automaticallyImplyLeading: false,
          flexibleSpace: Image.asset('assets/images/logo.png'),
        ),
      ),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Image.asset('assets/images/login_bg.png'),
          ),
          GetOTP(),
        ],
      ),
    );
  }
}
class GetOTP extends StatefulWidget {
  @override
  _GetOTPState createState() => _GetOTPState();

}
class _GetOTPState extends State<GetOTP> {
  List<TextEditingController> controllers = <TextEditingController>[new TextEditingController(),TextEditingController(),TextEditingController(),TextEditingController()];
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Container(
        height: SizeConfig.safeBlockVertical * 40,
        width: SizeConfig.safeBlockHorizontal * 90,
        padding: EdgeInsets.only(
          top: 10,
          right: 20,
          left: 20,
          bottom: 10,
        ),
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius:
          BorderRadius.vertical(bottom: Radius.circular(20)),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "NHẬP MÃ OTP",
              style: TextStyle(
                fontSize: SizeConfig.safeBlockHorizontal * 7,
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Text(
              "Xin Mời Bạn Nhập Mã OTP Chúng Tôi Đã Gửi Đến Số +84 796 664 119",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: SizeConfig.safeBlockHorizontal * 4,
              ),
            ),
            SizedBox(
              height: 10,
            ),
             Row(
             mainAxisAlignment: MainAxisAlignment.center,
             children: otpBoxs(50.0, controllers, Colors.white, Colors.black, context, false),
           ),
            SizedBox(
              height: 20,
            ),
            InkWell(
              onTap: () {},
              hoverColor: Colors.grey,
              child: Text("GỬI LẠI MÃ OTP", style: TextStyle(color: Colors.green, fontWeight: FontWeight.bold),),),
            SizedBox(
              height: 20,
            ),
            Text("OTP Có Hiệu Lực Trong 00:32", style: TextStyle(color: Colors.red),)
          ],
        ),
      ),
    );
  }
}


