import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';

class ChangePasswordScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(SizeConfig.safeBlockVertical * 20),
        child: AppBar(
          backgroundColor: Colors.green,
          automaticallyImplyLeading: false,
          flexibleSpace: Image.asset('assets/images/logo.png'),
        ),
      ),
      body: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: Image.asset('assets/images/login_bg.png'),
          ),
          Align(
        alignment: Alignment.topCenter,
        child: Container(
          height: SizeConfig.safeBlockVertical * 55,
          width: SizeConfig.safeBlockHorizontal * 90,
          padding: EdgeInsets.only(
            top: 10,
            right: 20,
            left: 20,
            bottom: 10,
          ),
          decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.vertical(bottom: Radius.circular(20)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("THAY ĐỔI MẬT KHẨU", style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 7,),),
              SizedBox(height: 30,),
              Text("Vui Lòng Nhập Mật Khẩu Mới Và Xác Nhận Mật Khẩu Mới Vào Ô Bên Dưới",textAlign: TextAlign.center ,style: TextStyle(fontSize: SizeConfig.safeBlockHorizontal * 4,),),
              SizedBox(height: 20,),
              Material(
                elevation: 3.0,
                shadowColor: Colors.grey[100],
                borderRadius: BorderRadius.circular(25.7),
                child: TextField(
                  autofocus: false,
                  style: TextStyle(
                      fontSize: 17.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    suffixIcon: Icon(
                        Icons.check_circle, color: Colors.green),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: 'Mật Khẩu Mới',
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.white),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.white),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10,),
              Material(
                elevation: 3.0,
                shadowColor: Colors.grey[100],
                borderRadius: BorderRadius.circular(25.7),
                child: TextField(
                  autofocus: false,
                  style: TextStyle(
                      fontSize: 17.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    suffixIcon: Icon(
                        Icons.check_circle, color: Colors.green),
                    filled: true,
                    fillColor: Colors.white,
                    hintText: 'Nhập Lại Mật Khẩu Mới',
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.white),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(
                          color: Colors.white),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 20,),
              ButtonTheme(
                minWidth: SizeConfig.safeBlockHorizontal * 90,
                child: RaisedButton(onPressed: () {},
                  color: Colors.green,
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(30.0)),
                  child: Text("Xác Nhận",
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold)
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
        ],
      ),
    );
  }
}
