import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/resource/models/models.dart';
import 'package:intl/intl.dart';
import '../../presentation.dart';

class AdminRevenueTab extends StatefulWidget {
  @override
  _AdminRevenueTabState createState() => _AdminRevenueTabState();

}

class _AdminRevenueTabState extends State<AdminRevenueTab> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            color: Colors.white,
            child: Column(
              children: [
                SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      '${DateFormat('H:m d/M/y').format(DateTime.now())}',
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Text(
                        "Thống Kê Doanh Thu",
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 8.0),
                      child: Container(
                          width: 40.0,
                          height: 40.0,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, color: Colors.black),
                          child: IconButton(
                            icon: Icon(
                              Icons.filter_alt,
                              color: Colors.white,
                            ),
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return Filter();
                                  });
                            },
                          )),
                    ),
                  ],
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  reverse: true,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                  child: CustomSelect(
                    buttons: _listWeek(14),
                    onChanged: (value) {},
                    defaultSelect: 0,
                    selectDecoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.yellow[600],
                          width: 2,
                        ),
                      ),
                    ),
                    selectStyle: TextStyle(
                        color: Colors.yellow[600],
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                    textStyle: TextStyle(
                      color: Colors.black54,
                      fontSize: 18,
                    ),
                    space: 35,
                  ),
                ),
                CustomDropdown(
                  shadow: true,
                  onChanged: (value) {},
                  items: [
                    DropdownMenuItem(
                      child: Text('Nhà hàng Thùy Trang - Huế'),
                    ),
                    DropdownMenuItem(
                      child: Text('Nhà hàng Rio- ĐN'),
                    ),
                    DropdownMenuItem(
                      child: Text('Nhà hàng Thắng Lợi - Phú Bài'),
                    ),
                  ],
                  hint: 'Chọn nhà hàng',
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
            color: Colors.black87,
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                       Icon(Icons.monetization_on, size: 30, color: Colors.white),
                        SizedBox(width: 4),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text("200 ", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 25)),
                                Text("TRIỆU", style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold)),
                              ],
                            ),
                            Text("Tổng Doanh Thu Ngày", style: TextStyle(color: Colors.white)),
                          ],
                        )
                      ],
                    ),
                    NumericTableChart(
                      isGrow: true,
                      title: '5%',
                      desc: 'Hôm Qua',
                    ),
                    NumericTableChart(
                      isGrow: false,
                      title: '2%',
                      desc: '1 Tháng Trước',
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                RoundedExpansionCard(
                  title: "BIỂU ĐỒ DOANH THU",
                  children: [
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text("ĐVT: TRIỆU", style: TextStyle(fontWeight: FontWeight.bold),),
                    ),
                    BarChartWeekly(
                      [
                        Bar(x: 0, y1: 1, y2: 2),
                        Bar(x: 1, y1: 5, y2: 2),
                        Bar(x: 2, y1: 2, y2: 3),
                        Bar(x: 3, y1: 1, y2: 2),
                        Bar(x: 4, y1: 1, y2: 2),
                        Bar(x: 5, y1: 2, y2: 1),
                        Bar(x: 6, y1: 1, y2: 2),
                      ],
                      columnWidth: 10,
                      bottomTitles: ['Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'CN'],
                      y1Color: Colors.greenAccent,
                      showGrid: true,
                      gridStep: 80,
                      divider: true,
                      y1Title: 'Tuần này',
                      y2Title: 'Tuần trước',
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                RoundedExpansionCard(
                  title: "BIỂU ĐỒ KHÁCH HÀNG",
                  children: [
                    Align(
                      alignment: Alignment.centerRight,
                      child: Text("ĐVT: KHÁCH HÀNG", style: TextStyle(fontWeight: FontWeight.bold),),
                    ),
                    BarChartWeekly(
                      [
                        Bar(x: 0, y1: 1, y2: 2),
                        Bar(x: 1, y1: 5, y2: 2),
                        Bar(x: 2, y1: 2, y2: 3),
                        Bar(x: 3, y1: 1, y2: 2),
                        Bar(x: 4, y1: 2, y2: 2),
                        Bar(x: 5, y1: 4, y2: 1),
                        Bar(x: 6, y1: 3, y2: 2),
                      ],
                      columnWidth: 10,
                      bottomTitles: ['Thứ 2', 'Thứ 3', 'Thứ 4', 'Thứ 5', 'Thứ 6', 'Thứ 7', 'CN'],
                      y1Color: Colors.indigoAccent,
                      showGrid: true,
                      gridStep: 80,
                      divider: true,
                      y1Title: 'Tuần này',
                      y2Title: 'Tuần trước',
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                RoundedExpansionCard(
                  title: "Tuơng Quan Khách Hàng/Doanh Thu",
                  children: [
                    LineChartWeek(
                      lines: [
                        Line(
                          spots: [
                            FlSpot(0, 510),
                            FlSpot(1, 520),
                            FlSpot(2, 300),
                            FlSpot(3, 310),
                            FlSpot(4, 305),
                            FlSpot(5, 300),
                            FlSpot(6, 400),
                          ],
                          color: Colors.blue,
                        ),
                        Line(
                          spots: [
                            FlSpot(0, 110),
                            FlSpot(1, 220),
                            FlSpot(2, 200),
                            FlSpot(3, 210),
                            FlSpot(4, 405),
                            FlSpot(5, 100),
                            FlSpot(6, 300),
                          ],
                          color: Colors.red,
                        ),
                        Line(
                          spots: [
                            FlSpot(0, 210),
                            FlSpot(1, 320),
                            FlSpot(2, 100),
                            FlSpot(3, 210),
                            FlSpot(4, 205),
                            FlSpot(5, 200),
                            FlSpot(6, 400),
                          ],
                          color: Colors.blue.withOpacity(0.4),
                        ),
                        Line(
                          spots: [
                            FlSpot(0, 310),
                            FlSpot(1, 120),
                            FlSpot(2, 300),
                            FlSpot(3, 510),
                            FlSpot(4, 205),
                            FlSpot(5, 100),
                            FlSpot(6, 400),
                          ],
                          color: Colors.red.withOpacity(0.4),
                        ),
                      ],

                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      "Khách Hàng Chi Tiêu Nhất",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    DropdownButtonHideUnderline(
                        child: DropdownButton(
                          hint: Text("TOP 5",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold)),
                          isExpanded: false,
                          icon: Icon(
                            Icons.keyboard_arrow_down,
                            color: Colors.white,
                            size: 30,
                          ),
                          onChanged: (value) {},
                          items: [
                            DropdownMenuItem(
                              child: Text("TOP 5"),
                              value: 5,
                            ),
                            DropdownMenuItem(
                              child: Text("TOP 10"),
                              value: 10,
                            ),
                            DropdownMenuItem(
                              child: Text("TOP 15"),
                              value: 5,
                            ),
                          ],
                          isDense: true,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    TopCustomerItem(
                        isFirst: true,
                        leading: "1",
                        title: "KEM",
                        trailing: "200",
                        description: "0796664119"),
                    TopCustomerItem(
                        isFirst: false,
                        leading: "2",
                        title: "HÒA",
                        trailing: "180",
                        description: "0796664119"),
                    TopCustomerItem(
                        isFirst: false,
                        leading: "3",
                        title: "FI ĐẠI",
                        trailing: "160",
                        description: "0796664119"),
                    TopCustomerItem(
                        isFirst: false,
                        leading: "4",
                        title: "BẰNG",
                        trailing: "140",
                        description: "0796664119"),
                    TopCustomerItem(
                        isFirst: false,
                        leading: "5",
                        title: "CƯỜNG",
                        trailing: "120",
                        description: "0796664119"),
                  ],
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ButtonTheme(
                      child: RaisedButton(
                        onPressed: () {
                          print('OK');
                        },
                        color: Colors.green,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        child: Container(
                          width: 250,
                          child: Center(
                            child: Text(
                              'XUẤT BÁO CÁO',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  _listWeek(int weeks) {
    final _list = <CustomSelectButton>[]..add(
      CustomSelectButton(
        child: Text('Tuần này'),
        value: 0,
      ),
    );
    var currentDate = DateTime.now();
    for (int i = 0; i < weeks; i++) {
      final endDate = currentDate.subtract(
        Duration(days: currentDate.weekday),
      );
      final startDate = endDate.subtract(
        Duration(days: DateTime.daysPerWeek),
      );
      currentDate = endDate;
      if (startDate.month == endDate.month) {
        _list.add(
          CustomSelectButton(
            child: Text(
                '${DateFormat('d').format(startDate)}-${DateFormat('d/M').format(endDate)}'),
            value: '${startDate.day}.${endDate.day}',
          ),
        );
      } else {
        _list.add(CustomSelectButton(
          child: Text(
              '${DateFormat('d/M').format(startDate)}-${DateFormat('d/M').format(endDate)}'),
          value: '${startDate.day}.${endDate.day}',
        ));
      }
    }

    return _list.reversed.toList();
  }
}
