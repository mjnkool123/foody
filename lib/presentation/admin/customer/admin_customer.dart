import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';
import 'package:foody/resource/models/models.dart';
import 'package:intl/intl.dart';

import '../../presentation.dart';

class AdminCustomerTab extends StatefulWidget {
  @override
  _AdminCustomerTabState createState() => _AdminCustomerTabState();
}

class _AdminCustomerTabState extends State<AdminCustomerTab> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            color: Colors.white,
            child: Column(
              children: [
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    '${DateFormat('H:m d/M/y').format(DateTime.now())}',
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Thống kê số lượng khách hàng',
                      style: TextStyle(fontSize: 18),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.black87,
                        shape: BoxShape.circle,
                      ),
                      child: IconButton(
                        icon: Icon(Icons.filter_alt),
                        onPressed: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return Filter();
                              });
                        },
                        color: Colors.white,
                        padding: EdgeInsets.all(6),
                        constraints: BoxConstraints(),
                      ),
                    ),
                  ],
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  reverse: true,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                  child: CustomSelect(
                    buttons: _listDate(7),
                    onChanged: (value) {},
                    defaultSelect: 0,
                    selectDecoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.yellow[600],
                          width: 2,
                        ),
                      ),
                    ),
                    selectStyle: TextStyle(
                        color: Colors.yellow[600],
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                    textStyle: TextStyle(
                      color: Colors.black54,
                      fontSize: 18,
                    ),
                    space: 35,
                  ),
                ),
                SizedBox(height: 10),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
            color: Colors.black87,
            child: Column(
              children: [
                SizedBox(
                  height: 10,
                ),
                CustomDropdown(
                  shadow: true,
                  onChanged: (value) {},
                  items: [
                    DropdownMenuItem(
                      child: Text('Nhà hàng Thùy Trang - Huế'),
                    ),
                    DropdownMenuItem(
                      child: Text('Nhà hàng Rio- ĐN'),
                    ),
                    DropdownMenuItem(
                      child: Text('Nhà hàng Thắng Lợi - Phú Bài'),
                    ),
                  ],
                  hint: 'Chọn nhà hàng',
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        NumericTableChart(
                          icon: Icon(
                            Icons.person,
                            color: Colors.white,
                          ),
                          title: '250',
                          desc: 'Khách hàng',
                        ),
                        NumericTableChart(
                          icon: Icon(
                            Icons.person,
                            color: Colors.transparent,
                          ),
                          title: '200',
                          desc: 'Tỷ Lệ Khách Đến',
                        ),
                        NumericTableChart(
                          icon: Icon(
                            Icons.person,
                            color: Colors.transparent,
                          ),
                          title: '50',
                          desc: 'Tỷ Lệ Khách Hủy',
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        NumericTableChart(
                          isGrow: true,
                          title: '5%',
                          desc: 'Hôm Qua',
                        ),
                        NumericTableChart(
                          isGrow: true,
                          title: '5%',
                          desc: 'Hôm Qua',
                        ),
                        NumericTableChart(
                          isGrow: true,
                          title: '5%',
                          desc: 'Hôm Qua',
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        NumericTableChart(
                          isGrow: false,
                          title: '2%',
                          desc: '1 Tháng Trước',
                        ),
                        NumericTableChart(
                          isGrow: false,
                          title: '2%',
                          desc: '1 Tháng Trước',
                        ),
                        NumericTableChart(
                          isGrow: false,
                          title: '2%',
                          desc: '1 Tháng Trước',
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  children: [
                    Flexible(
                      flex: 1,
                      child: RoundedExpansionCard(
                        title: 'Số lượng đặt hàng',
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              '20',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                          ),
                          BarChartWeekly(
                            [
                              Bar(x: 0, y1: 1, y2: 2),
                              Bar(x: 1, y1: 2, y2: 3),
                              Bar(x: 2, y1: 1, y2: 2),
                              Bar(x: 3, y1: 1, y2: 2),
                              Bar(x: 3, y1: 2, y2: 1),
                              Bar(x: 5, y1: 1, y2: 2),
                              Bar(x: 2, y1: 5, y2: 2),
                            ],
                            bottomTitles: [
                              'T2',
                              'T3',
                              'T4',
                              'T5',
                              'T6',
                              'T7',
                              'CN'
                            ],
                            y1Color: Colors.greenAccent,
                            y1Title: 'Tuần này',
                            y2Title: 'Tuần trước',
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Flexible(
                      flex: 1,
                      child: RoundedExpansionCard(
                        title: 'Tỷ Lệ Lấp Đầy Bàn',
                        children: [
                          Align(
                            alignment: Alignment.topLeft,
                            child: Text(
                              '20',
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                          ),
                          BarChartWeekly(
                            [
                              Bar(x: 0, y1: 1, y2: 2),
                              Bar(x: 1, y1: 2, y2: 3),
                              Bar(x: 2, y1: 1, y2: 2),
                              Bar(x: 4, y1: 5, y2: 2),
                              Bar(x: 3, y1: 2, y2: 1),
                              Bar(x: 5, y1: 1, y2: 2),
                              Bar(x: 2, y1: 4, y2: 3),
                            ],
                            bottomTitles: [
                              'T2',
                              'T3',
                              'T4',
                              'T5',
                              'T6',
                              'T7',
                              'CN'
                            ],
                            y1Color: Colors.yellow[800],
                            y1Title: 'Tuần này',
                            y2Title: 'Tuần trước',
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedExpansionCard(
                  title: 'Khách Hàng Mới Tuần Này',
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        '50',
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: BarChartWeekly(
                            [
                              Bar(x: 0, y1: 1, y2: 2),
                              Bar(x: 1, y1: 2, y2: 3),
                              Bar(x: 2, y1: 1, y2: 2),
                              Bar(x: 2, y1: 1, y2: 2),
                              Bar(x: 3, y1: 2, y2: 1),
                              Bar(x: 5, y1: 1, y2: 2),
                              Bar(x: 2, y1: 5, y2: 2),
                            ],
                            bottomTitles: [
                              'T2',
                              'T3',
                              'T4',
                              'T5',
                              'T6',
                              'T7',
                              'CN'
                            ],
                            y1Color: Colors.indigoAccent,
                            y1Title: 'Tuần này',
                            y2Title: 'Tuần trước',
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Flexible(
                          flex: 1,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Icon(Icons.arrow_upward_rounded,
                                      color: Colors.white),
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                            text: '25% \n',
                                            style: TextStyle(
                                                fontSize: 25,
                                                color: Colors.black)),
                                        TextSpan(
                                            text: 'Tổng Số Khách',
                                            style:
                                                TextStyle(color: Colors.black)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 30),
                              Row(
                                children: [
                                  Icon(Icons.arrow_downward_rounded,
                                      color: Colors.red),
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                            text: '5% \n',
                                            style: TextStyle(
                                                fontSize: 25,
                                                color: Colors.black)),
                                        TextSpan(
                                            text: 'Tuần Trước',
                                            style:
                                                TextStyle(color: Colors.black)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedExpansionCard(
                  title: 'Khách Lần Đầu Trở Lại',
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        '100',
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: BarChartWeekly(
                            [
                              Bar(x: 0, y1: 1, y2: 2),
                              Bar(x: 1, y1: 2, y2: 3),
                              Bar(x: 2, y1: 1, y2: 2),
                              Bar(x: 2, y1: 1, y2: 2),
                              Bar(x: 3, y1: 2, y2: 1),
                              Bar(x: 5, y1: 1, y2: 2),
                              Bar(x: 2, y1: 5, y2: 2),
                            ],
                            bottomTitles: [
                              'T2',
                              'T3',
                              'T4',
                              'T5',
                              'T6',
                              'T7',
                              'CN'
                            ],
                            y1Color: Colors.red,
                            y1Title: 'Tuần này',
                            y2Title: 'Tuần trước',
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Flexible(
                          flex: 1,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Icon(Icons.arrow_upward_rounded,
                                      color: Colors.white),
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                            text: '50% \n',
                                            style: TextStyle(
                                                fontSize: 25,
                                                color: Colors.black)),
                                        TextSpan(
                                            text: 'Tổng Số Khách',
                                            style:
                                                TextStyle(color: Colors.black)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 30),
                              Row(
                                children: [
                                  Icon(Icons.arrow_upward_rounded,
                                      color: Colors.greenAccent),
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                            text: '5% \n',
                                            style: TextStyle(
                                                fontSize: 25,
                                                color: Colors.black)),
                                        TextSpan(
                                            text: 'Tuần Trước',
                                            style:
                                                TextStyle(color: Colors.black)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedExpansionCard(
                  title: 'Khách Cũ Trở Lại',
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        '50',
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      children: [
                        Flexible(
                          flex: 1,
                          child: BarChartWeekly(
                            [
                              Bar(x: 0, y1: 1, y2: 2),
                              Bar(x: 1, y1: 2, y2: 3),
                              Bar(x: 2, y1: 1, y2: 2),
                              Bar(x: 2, y1: 1, y2: 2),
                              Bar(x: 3, y1: 2, y2: 1),
                              Bar(x: 5, y1: 1, y2: 2),
                              Bar(x: 2, y1: 5, y2: 2),
                            ],
                            bottomTitles: [
                              'T2',
                              'T3',
                              'T4',
                              'T5',
                              'T6',
                              'T7',
                              'CN'
                            ],
                            y1Color: Colors.yellow[800],
                            y1Title: 'Tuần này',
                            y2Title: 'Tuần trước',
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Flexible(
                          flex: 1,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Icon(Icons.arrow_upward_rounded,
                                      color: Colors.white),
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                            text: '25% \n',
                                            style: TextStyle(
                                                fontSize: 25,
                                                color: Colors.black)),
                                        TextSpan(
                                            text: 'Tổng Số Khách',
                                            style:
                                                TextStyle(color: Colors.black)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(height: 30),
                              Row(
                                children: [
                                  Icon(Icons.arrow_upward_rounded,
                                      color: Colors.greenAccent),
                                  RichText(
                                    text: TextSpan(
                                      children: [
                                        TextSpan(
                                            text: '5% \n',
                                            style: TextStyle(
                                                fontSize: 25,
                                                color: Colors.black)),
                                        TextSpan(
                                            text: 'Tuần Trước',
                                            style:
                                                TextStyle(color: Colors.black)),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      "Khách Hàng Chi Tiêu Nhất",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold),
                    ),
                    DropdownButtonHideUnderline(
                        child: DropdownButton(
                      hint: Text("TOP 5",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold)),
                      isExpanded: false,
                      icon: Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.white,
                        size: 30,
                      ),
                      onChanged: (value) {},
                      items: [
                        DropdownMenuItem(
                          child: Text("TOP 5"),
                          value: 5,
                        ),
                        DropdownMenuItem(
                          child: Text("TOP 10"),
                          value: 10,
                        ),
                        DropdownMenuItem(
                          child: Text("TOP 15"),
                          value: 5,
                        ),
                      ],
                      isDense: true,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Column(
                  children: [
                    TopCustomerItem(
                        isFirst: true,
                        leading: "1",
                        title: "KEM",
                        trailing: "200",
                        description: "0796664119"),
                    TopCustomerItem(
                        isFirst: false,
                        leading: "2",
                        title: "HÒA",
                        trailing: "180",
                        description: "0796664119"),
                    TopCustomerItem(
                        isFirst: false,
                        leading: "3",
                        title: "FI ĐẠI",
                        trailing: "160",
                        description: "0796664119"),
                    TopCustomerItem(
                        isFirst: false,
                        leading: "4",
                        title: "BẰNG",
                        trailing: "140",
                        description: "0796664119"),
                    TopCustomerItem(
                        isFirst: false,
                        leading: "5",
                        title: "CƯỜNG",
                        trailing: "120",
                        description: "0796664119"),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                DualPieChartWidget(
                  title: "Thống Kê Khách Hàng Theo Khu Vực",
                  pie1: [
                    Pie(
                      value: 65,
                      title: '65%',
                      note: 'Hà Nội',
                      color: Colors.greenAccent,
                    ),
                    Pie(
                      value: 35,
                      title: '35%',
                      note: 'Toàn Quốc',
                      color: Colors.black,
                    ),
                  ],
                  pie2: [
                    Pie(
                      value: 5,
                      title: '5%',
                      note: 'Hà Nội',
                      color: Colors.greenAccent,
                    ),
                    Pie(
                      value: 95,
                      title: '95%',
                      note: 'Toàn Quốc',
                      color: Colors.black,
                    ),
                  ],
                  pie1Title: "Hôm Nay",
                  pie2Title: "Hôm Qua",
                ),
                SizedBox(
                  height: 20,
                ),
                DualPieChartWidget(
                  title: "Phân Loại Khách Hàng Theo Hạng",
                  pie1: [
                    Pie(
                      value: 50,
                      title: '50%',
                      note: 'VIP 1',
                      color: Colors.yellow[800],
                    ),
                    Pie(
                      value: 25,
                      title: '25%',
                      note: 'VIP 2',
                      color: Colors.greenAccent,
                    ),
                    Pie(
                      value: 25,
                      title: '25%',
                      note: 'VIP 3',
                      color: Colors.black,
                    ),
                  ],
                  pie2: [
                    Pie(
                      value: 45,
                      title: '45%',
                      note: 'VIP 1',
                      color: Colors.yellow[800],
                    ),
                    Pie(
                      value: 25,
                      title: '25%',
                      note: 'VIP 2',
                      color: Colors.greenAccent,
                    ),
                    Pie(
                      value: 30,
                      title: '30%',
                      note: 'VIP 3',
                      color: Colors.black,
                    ),
                  ],
                  pie1Title: "Hôm Nay",
                  pie2Title: "Hôm Qua",
                ),
                SizedBox(
                  height: 20,
                ),
                RoundedExpansionCard(
                  title: 'Thống Kê Theo Độ Tuổi (Membership)',
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Text(
                        'ĐVT: Khách Hàng',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: BarChartWeekly(
                        [
                          Bar(x: 0, y1: 150, y2: 80),
                          Bar(x: 1, y1: 190, y2: 150),
                          Bar(x: 2, y1: 400, y2: 80),
                          Bar(x: 3, y1: 190, y2: 60),
                          Bar(x: 4, y1: 110, y2: 190),
                        ],
                        bottomTitles: [
                          '<18',
                          '18-25',
                          '25-35',
                          '35-50',
                          '>50',
                        ],
                        height: 200,
                        showGrid: true,
                        y1Title: 'Hôm Nay',
                        y1Color: Colors.greenAccent,
                        y2Title: 'Hôm Qua',
                        unit: 'Triệu',
                        gridStep: 100,
                        divider: true,
                        columnWidth: 12,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                DualPieChartWidget(
                  title: "Thống Kê Theo Độ Tuổi (Membership)",
                  pie1: [
                    Pie(
                      value: 35,
                      title: '35%',
                      note: 'NỮ',
                      color: Colors.greenAccent,
                    ),
                    Pie(
                      value: 65,
                      title: '65%',
                      note: 'NAM',
                      color: Colors.black,
                    ),
                  ],
                  pie2: [
                    Pie(
                      value: 5,
                      title: '5%',
                      note: 'NỮ',
                      color: Colors.greenAccent,
                    ),
                    Pie(
                      value: 95,
                      title: '95%',
                      note: 'NAM',
                      color: Colors.black,
                    ),
                  ],
                  pie1Title: "Hôm Nay",
                  pie2Title: "Hôm Qua",
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ButtonTheme(
                      child: RaisedButton(
                        onPressed: () {
                          print('OK');
                        },
                        color: Colors.green,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        child: Container(
                          width: 250,
                          child: Center(
                            child: Text(
                              'XUẤT BÁO CÁO',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _listDate(int length) {
    final _widgets = <CustomSelectButton>[];
    final currentDate = DateTime.now();
    _widgets.add(CustomSelectButton(
      child: Text('Hôm nay'),
      value: 0,
    ));
    for (var i = 1; i <= length; i++) {
      final date = currentDate.subtract(Duration(days: i));
      _widgets.add(CustomSelectButton(
        child: Text(date.day.toString()),
        value: date.millisecondsSinceEpoch,
      ));
    }
    return _widgets.reversed.toList();
  }
}

class DualPieChartWidget extends StatelessWidget {
  final String title;
  final List<Pie> pie1;
  final String pie1Title;
  final List<Pie> pie2;
  final String pie2Title;

  const DualPieChartWidget(
      {Key key,
      this.title,
      this.pie1,
      this.pie1Title,
      this.pie2,
      this.pie2Title})
      : super(key: key);

  _dotInit() {
    final _name = <DotWidget>[];
    final _map = <Color, String>{};
    pie1.forEach((element) {
      if (!_map.containsValue(element.color)) {
        _map.addAll({
          element.color: element.note,
        });
      }
    });
    pie2.forEach((element) {
      if (!_map.containsValue(element.color)) {
        _map.addAll({
          element.color: element.note,
        });
      }
    });
    _map.forEach((key, value) {
      _name.add(DotWidget(
        text: value,
        color: key,
      ));
    });
    return _name;
  }

  @override
  Widget build(BuildContext context) {
    return RoundedExpansionCard(
      title: title ?? 'null',
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Column(
              children: [
                PieChartCustom(
                  pies: pie1,
                ),
                SizedBox(height: 10),
                Text(pie1Title ?? 'null'),
              ],
            ),
            Column(
              children: [
                PieChartCustom(
                  pies: pie2,
                ),
                SizedBox(height: 10),
                Text(pie2Title ?? 'null'),
              ],
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10),
          child: Divider(color: Colors.black87.withOpacity(0.4)),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: _dotInit(),
        )
      ],
    );
  }
}
