import 'package:flutter/material.dart';
import 'package:foody/resource/models/models.dart';

import '../../../presentation.dart';

class PlaceCustomerTab extends StatefulWidget {

  @override
  _PlaceCustomerTabState createState() => _PlaceCustomerTabState();

}

class _PlaceCustomerTabState extends State<PlaceCustomerTab> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        color: Colors.black87,
        child: Column(
          children: [
            CustomDropdown(
              shadow: true,
              onChanged: (value) {},
              items: [
                DropdownMenuItem(
                  child: Text('Nhà hàng Thùy Trang - Huế'),
                ),
                DropdownMenuItem(
                  child: Text('Nhà hàng Rio- ĐN'),
                ),
                DropdownMenuItem(
                  child: Text('Nhà hàng Thắng Lợi - Phú Bài'),
                ),
              ],
              hint: 'Chọn nhà hàng',
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(2),
                  child: Icon(
                    Icons.person,
                    color: Colors.white,
                    size: 25,
                  ),
                ),
                RichText(
                  text: TextSpan(
                    children: [
                      TextSpan(
                        text: '250 ',
                        style: TextStyle(
                            fontSize: 25,
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      TextSpan(
                        text: ' \n',
                        style: TextStyle(
                            fontWeight: FontWeight.bold
                        ),
                      ),
                      TextSpan(
                        text: 'Khách Đặt Bàn',
                      ),
                    ],
                  ),
                ),
                NumericTableChart(
                  isGrow: true,
                  title: '5%',
                  desc: 'Hôm Qua',
                ),
                NumericTableChart(
                  isGrow: false,
                  title: '2%',
                  desc: '1 Tháng Trước',
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Flexible(
                  flex: 1,
                  child: RoundedExpansionCard(
                    title: 'Quận Ba Đình',
                    children: [
                      PieChartCustom(
                        pies: [
                          Pie(
                            color: Colors.greenAccent,
                            value: 65,
                            title: "65%"
                          ),
                          Pie(
                              color: Colors.black,
                              value: 35,
                              title: "35%"
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Flexible(
                  flex: 1,
                  child: RoundedExpansionCard(
                    title: 'Hà Nội',
                    children: [
                      PieChartCustom(
                        pies: [
                          Pie(
                              color: Colors.greenAccent,
                              value: 35,
                              title: "35%"
                          ),
                          Pie(
                              color: Colors.black,
                              value: 65,
                              title: "65%"
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            RoundedExpansionCard(
              title: 'Toàn Quốc',
              children: [
                PieChartCustom(
                  pies: [
                    Pie(
                        color: Colors.red,
                        value: 5,
                        title: "5%"
                    ),
                    Pie(
                        color: Colors.black,
                        value: 95,
                        title: "95%"
                    )
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            RoundedExpansionCard(
              title: 'So Sánh Nhà Hàng Trong Cùng Quận',
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Text(
                    'ĐVT: Lượt Khách',
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                BarChartWeekly(
                  [
                    Bar(x: 0, y1: 50, y2: 100),
                    Bar(x: 1, y1: 190, y2: 150),
                    Bar(x: 2, y1: 400, y2: 80),
                    Bar(x: 3, y1: 60, y2: 100),
                    Bar(x: 4, y1: 200, y2: 80),
                    Bar(x: 5, y1: 250, y2: 200),
                  ],
                  bottomTitles: [
                    'Cơ Sở 1',
                    'Cơ Sở 2',
                    'Cơ Sở 3',
                    'Cơ Sở 4',
                    'Cơ Sở 5',
                    'Cơ Sở 6',
                  ],
                    height: 200,
                    showGrid: true,
                    y1Title: 'Tuần Này',
                    y1Color: Colors.greenAccent,
                    y2Title: 'Tuần Trước',
                    unit: 'Triệu',
                    gridStep: 100,
                    divider: true,
                    columnWidth: 12,
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            RoundedExpansionCard(
              title: 'So Sánh Giữa Các Quận',
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Text(
                    'ĐVT: Lượt Khách',
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                BarChartWeekly(
                  [
                    Bar(x: 0, y1: 100, y2: 50),
                    Bar(x: 1, y1: 190, y2: 150),
                    Bar(x: 2, y1: 400, y2: 80),
                    Bar(x: 3, y1: 300, y2: 100),
                  ],
                  bottomTitles: [
                    'Quận A',
                    'Quận B',
                    'Quận C',
                    'Quận D',
                  ],
                  height: 200,
                  showGrid: true,
                  y1Title: 'Tuần Này',
                  y1Color: Colors.greenAccent,
                  y2Title: 'Tuần Trước',
                  unit: 'Triệu',
                  gridStep: 100,
                  divider: true,
                  columnWidth: 12,
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            RoundedExpansionCard(
              title: 'So Sánh Giữa Các Thành Phố',
              children: [
                Align(
                  alignment: Alignment.topRight,
                  child: Text(
                    'ĐVT: Lượt Khách',
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                BarChartWeekly(
                  [
                    Bar(x: 0, y1: 100, y2: 50),
                    Bar(x: 1, y1: 190, y2: 150),
                    Bar(x: 2, y1: 400, y2: 80),
                    Bar(x: 3, y1: 300, y2: 100),
                  ],
                  bottomTitles: [
                    'Hà Nội',
                    'Huế',
                    'Đà Nẵng',
                    'Đà Lạt',
                  ],
                  height: 200,
                  showGrid: true,
                  y1Title: 'Tuần Này',
                  y1Color: Colors.greenAccent,
                  y2Title: 'Tuần Trước',
                  unit: 'Triệu',
                  gridStep: 100,
                  divider: true,
                  columnWidth: 12,
                ),
              ],
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ButtonTheme(
                  child: RaisedButton(
                    onPressed: () {
                      print('OK');
                    },
                    color: Colors.green,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Container(
                      width: 250,
                      child: Center(
                        child: Text(
                          'XUẤT BÁO CÁO',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}