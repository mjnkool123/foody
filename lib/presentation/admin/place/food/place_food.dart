import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/presentation/presentation.dart';
import 'package:md2_tab_indicator/md2_tab_indicator.dart';

class PlaceFoodTab extends StatefulWidget {
  @override
  _PlaceFoodTabState createState() => _PlaceFoodTabState();

}

class _PlaceFoodTabState extends State<PlaceFoodTab> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.black87,
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(70),
              child: Column(
                children: [
                  CustomDropdown(
                    shadow: true,
                    onChanged: (value) {},
                    items: [
                      DropdownMenuItem(
                        child: Text('Nhà hàng Thùy Trang - Huế'),
                      ),
                      DropdownMenuItem(
                        child: Text('Nhà hàng Rio- ĐN'),
                      ),
                      DropdownMenuItem(
                        child: Text('Nhà hàng Thắng Lợi - Phú Bài'),
                      ),
                    ],
                    hint: 'Chọn nhà hàng',
                  ),
                  TabBar(
                    indicator: MD2Indicator(
                      indicatorSize: MD2IndicatorSize.normal,
                      indicatorHeight: 5,
                      indicatorColor: Colors.red,
                    ),
                    labelPadding: EdgeInsets.all(2.0),
                    indicatorSize: TabBarIndicatorSize.label,
                    labelColor: Colors.red,
                    unselectedLabelColor: Colors.grey,
                    tabs: [
                      Tab(
                          child: Text("LƯỢT ĐẶT",
                              style: TextStyle(fontWeight: FontWeight.bold))),
                      Tab(
                          child: Text("DOANH THU",
                              style: TextStyle(fontWeight: FontWeight.bold))),
                      Tab(
                          child: Text("LÀM LẠI",
                              style: TextStyle(fontWeight: FontWeight.bold))),
                    ],
                  ),
                  SizedBox(height: 10,)
                ],
              ),
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              PlaceFoodBookingTab(),
              Center(child: Text("null")),
              Center(child: Text("null")),
            ],
          ),
        ));
  }
}