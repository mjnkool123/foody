import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/resource/models/models.dart';

import '../../../../presentation.dart';

class PlaceFoodBookingTab extends StatefulWidget{
  @override
  _PlaceFoodBookingTab createState() => _PlaceFoodBookingTab();

}

class _PlaceFoodBookingTab extends State<PlaceFoodBookingTab> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        color: Colors.black87,
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SortButton(
                  onChanged: (v) {},
                ),
                DropdownButtonHideUnderline(
                    child: DropdownButton(
                      hint: Text("TOP 5", style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),
                      isExpanded: false,
                      icon: Icon(Icons.keyboard_arrow_down, color: Colors.white, size: 30,),
                      onChanged: (value) {},
                      items: [
                        DropdownMenuItem(child: Text("TOP 5"),
                          value: 5,
                        ),
                        DropdownMenuItem(child: Text("TOP 10"),
                          value: 10,
                        ),
                        DropdownMenuItem(child: Text("TOP 15"),
                          value: 5,
                        ),
                      ],
                      isDense: true,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                      ),
                    )),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            TopFoodItem(
              isFirst: true,
              leading: "1",
              title: "TÔM HÙM A",
              trailing: "200",
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RoundedExpansionCard(
                    title: "Thống Kê Theo Ngày",
                    children: [
                      Row(
                        children: [
                          Flexible(
                              flex: 1,
                              child: Column(
                                children: [
                                  DotWidget(
                                    color: Colors.greenAccent,
                                    text: "Hôm Nay",
                                  ),
                                  DotWidget(
                                    color: Colors.grey,
                                    text: "Hôm Qua",
                                  ),
                                ],
                              )
                          ),
                          Flexible(
                            flex: 1,
                            child: BarChartWeekly(
                              [
                                Bar(x: 0, y1: 200, y2: 100),
                              ],
                              height: 80,
                              showDot: false,
                              showGrid: true,
                              gridStep: 30,
                              y1Color: Colors.greenAccent,
                              unit: "Lượt",
                              columnWidth: 8,
                            ),)
                        ],
                      )
                    ],
                  ),
                ),
                RoundedExpansionCard(
                  title: 'Toàn Quốc',
                  children: [
                    PieChartCustom(
                      pies: [
                        Pie(
                            color: Colors.greenAccent,
                            value: 5,
                            title: "5%"
                        ),
                        Pie(
                            color: Colors.black,
                            value: 95,
                            title: "95%"
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
            TopFoodItem(
              isFirst: false,
              leading: "2",
              title: "TÔM HÙM B",
              trailing: "180",
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RoundedExpansionCard(
                    title: "Thống Kê Theo Ngày",
                    children: [
                      Row(
                        children: [
                          Flexible(
                              flex: 1,
                              child: Column(
                                children: [
                                  DotWidget(
                                    color: Colors.greenAccent,
                                    text: "Hôm Nay",
                                  ),
                                  DotWidget(
                                    color: Colors.grey,
                                    text: "Hôm Qua",
                                  ),
                                ],
                              )
                          ),
                          Flexible(
                            flex: 1,
                            child: BarChartWeekly(
                              [
                                Bar(x: 0, y1: 200, y2: 100),
                              ],
                              height: 80,
                              showDot: false,
                              showGrid: true,
                              gridStep: 30,
                              y1Color: Colors.greenAccent,
                              unit: "Lượt",
                              columnWidth: 8,
                            ),)
                        ],
                      )
                    ],
                  ),
                ),
                RoundedExpansionCard(
                  title: 'Toàn Quốc',
                  children: [
                    PieChartCustom(
                      pies: [
                        Pie(
                            color: Colors.greenAccent,
                            value: 5,
                            title: "5%"
                        ),
                        Pie(
                            color: Colors.black,
                            value: 95,
                            title: "95%"
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
            TopFoodItem(
              isFirst: false,
              leading: "3",
              title: "TÔM HÙM C",
              trailing: "160",
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RoundedExpansionCard(
                    title: "Thống Kê Theo Ngày",
                    children: [
                      Row(
                        children: [
                          Flexible(
                              flex: 1,
                              child: Column(
                                children: [
                                  DotWidget(
                                    color: Colors.greenAccent,
                                    text: "Hôm Nay",
                                  ),
                                  DotWidget(
                                    color: Colors.grey,
                                    text: "Hôm Qua",
                                  ),
                                ],
                              )
                          ),
                          Flexible(
                            flex: 1,
                            child: BarChartWeekly(
                              [
                                Bar(x: 0, y1: 200, y2: 100),
                              ],
                              height: 80,
                              showDot: false,
                              showGrid: true,
                              gridStep: 30,
                              y1Color: Colors.greenAccent,
                              unit: "Lượt",
                              columnWidth: 8,
                            ),)
                        ],
                      )
                    ],
                  ),
                ),
                RoundedExpansionCard(
                  title: 'Toàn Quốc',
                  children: [
                    PieChartCustom(
                      pies: [
                        Pie(
                            color: Colors.greenAccent,
                            value: 5,
                            title: "5%"
                        ),
                        Pie(
                            color: Colors.black,
                            value: 95,
                            title: "95%"
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
            TopFoodItem(
              isFirst: false,
              leading: "4",
              title: "TÔM HÙM D",
              trailing: "140",
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RoundedExpansionCard(
                    title: "Thống Kê Theo Ngày",
                    children: [
                      Row(
                        children: [
                          Flexible(
                              flex: 1,
                              child: Column(
                                children: [
                                  DotWidget(
                                    color: Colors.greenAccent,
                                    text: "Hôm Nay",
                                  ),
                                  DotWidget(
                                    color: Colors.grey,
                                    text: "Hôm Qua",
                                  ),
                                ],
                              )
                          ),
                          Flexible(
                            flex: 1,
                            child: BarChartWeekly(
                              [
                                Bar(x: 0, y1: 200, y2: 100),
                              ],
                              height: 80,
                              showDot: false,
                              showGrid: true,
                              gridStep: 30,
                              y1Color: Colors.greenAccent,
                              unit: "Lượt",
                              columnWidth: 8,
                            ),)
                        ],
                      )
                    ],
                  ),
                ),
                RoundedExpansionCard(
                  title: 'Toàn Quốc',
                  children: [
                    PieChartCustom(
                      pies: [
                        Pie(
                            color: Colors.greenAccent,
                            value: 5,
                            title: "5%"
                        ),
                        Pie(
                            color: Colors.black,
                            value: 95,
                            title: "95%"
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
            TopFoodItem(
              isFirst: false,
              leading: "5",
              title: "TÔM HÙM A",
              trailing: "120",
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RoundedExpansionCard(
                    title: "Thống Kê Theo Ngày",
                    children: [
                      Row(
                        children: [
                          Flexible(
                              flex: 1,
                              child: Column(
                                children: [
                                  DotWidget(
                                    color: Colors.greenAccent,
                                    text: "Hôm Nay",
                                  ),
                                  DotWidget(
                                    color: Colors.grey,
                                    text: "Hôm Qua",
                                  ),
                                ],
                              )
                          ),
                          Flexible(
                            flex: 1,
                            child: BarChartWeekly(
                              [
                                Bar(x: 0, y1: 200, y2: 100),
                              ],
                              height: 80,
                              showDot: false,
                              showGrid: true,
                              gridStep: 30,
                              y1Color: Colors.greenAccent,
                              unit: "Lượt",
                              columnWidth: 8,
                            ),)
                        ],
                      )
                    ],
                  ),
                ),
                RoundedExpansionCard(
                  title: 'Toàn Quốc',
                  children: [
                    PieChartCustom(
                      pies: [
                        Pie(
                            color: Colors.greenAccent,
                            value: 5,
                            title: "5%"
                        ),
                        Pie(
                            color: Colors.black,
                            value: 95,
                            title: "95%"
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: ButtonTheme(
                  child: RaisedButton(
                    onPressed: () {
                      print('OK');
                    },
                    color: Colors.green,
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Container(
                      width: 250,
                      child: Center(
                        child: Text(
                          'XUẤT BÁO CÁO',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}