import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:md2_tab_indicator/md2_tab_indicator.dart';

import '../../presentation.dart';

class AdminPlaceTab extends StatefulWidget {
  @override
  _AdminPlaceTabState createState() => _AdminPlaceTabState();
}

class _AdminPlaceTabState extends State<AdminPlaceTab> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            elevation: 1,
            backgroundColor: Colors.white,
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(150),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        '${DateFormat('H:m d/M/y').format(DateTime.now())}',
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          "So Sánh Trong Hệ Thống",
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Container(
                            width: 40.0,
                            height: 40.0,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: Colors.black),
                            child: IconButton(
                              icon: Icon(
                                Icons.filter_alt,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return Filter();
                                    });
                              },
                            )),
                      ),
                    ],
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    reverse: true,
                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                    child: CustomSelect(
                      buttons: _listWeek(4),
                      onChanged: (value) {},
                      defaultSelect: 0,
                      selectDecoration: BoxDecoration(
                        border: Border(
                          bottom: BorderSide(
                            color: Colors.yellow[600],
                            width: 2,
                          ),
                        ),
                      ),
                      selectStyle: TextStyle(
                          color: Colors.yellow[600],
                          fontSize: 18,
                          fontWeight: FontWeight.w600),
                      textStyle: TextStyle(
                        color: Colors.black54,
                        fontSize: 18,
                      ),
                      space: 35,
                    ),
                  ),
                  TabBar(
                    indicator: MD2Indicator(
                      indicatorSize: MD2IndicatorSize.normal,
                      indicatorHeight: 5,
                      indicatorColor: Colors.red,
                    ),
                    labelPadding: EdgeInsets.all(2.0),
                    indicatorSize: TabBarIndicatorSize.label,
                    labelColor: Colors.red,
                    unselectedLabelColor: Colors.grey,
                    tabs: [
                      Tab(
                          child: Text("KHÁCH HÀNG",
                              style: TextStyle(fontWeight: FontWeight.bold))),
                      Tab(
                          child: Text("MÓN ĂN",
                              style: TextStyle(fontWeight: FontWeight.bold))),
                      Tab(
                          child: Text("DOANH THU",
                              style: TextStyle(fontWeight: FontWeight.bold))),
                    ],
                  ),
                  SizedBox(height: 20)
                ],
              ),
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              PlaceCustomerTab(),
              PlaceFoodTab(),
              PlaceRevenueTab(),
            ],
          ),
        ));
  }
  _listWeek(int weeks) {
    final _list = <CustomSelectButton>[]..add(
      CustomSelectButton(
        child: Text('Tuần này'),
        value: 0,
      ),
    );
    var currentDate = DateTime.now();
    for (int i = 0; i < weeks; i++) {
      final endDate = currentDate.subtract(
        Duration(days: currentDate.weekday),
      );
      final startDate = endDate.subtract(
        Duration(days: DateTime.daysPerWeek),
      );
      currentDate = endDate;
      if (startDate.month == endDate.month) {
        _list.add(
          CustomSelectButton(
            child: Text(
                '${DateFormat('d').format(startDate)}-${DateFormat('d/M').format(endDate)}'),
            value: '${startDate.day}.${endDate.day}',
          ),
        );
      } else {
        _list.add(CustomSelectButton(
          child: Text(
              '${DateFormat('d/M').format(startDate)}-${DateFormat('d/M').format(endDate)}'),
          value: '${startDate.day}.${endDate.day}',
        ));
      }
    }

    return _list.reversed.toList();
  }
}