import 'package:flutter/material.dart';
import 'package:foody/resource/models/models.dart';
import 'package:intl/intl.dart';

import '../../../presentation.dart';

class RevenueTab extends StatefulWidget {
  @override
  _RevenueTabState createState() => _RevenueTabState();

}

class _RevenueTabState extends State<RevenueTab> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            color: Colors.white,
            child: Column(
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  reverse: true,
                  padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                  child: CustomSelect(
                    buttons: _listWeek(14),
                    onChanged: (value) {},
                    defaultSelect: 0,
                    selectDecoration: BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Colors.yellow[600],
                          width: 2,
                        ),
                      ),
                    ),
                    selectStyle: TextStyle(
                        color: Colors.yellow[600],
                        fontSize: 18,
                        fontWeight: FontWeight.w600),
                    textStyle: TextStyle(
                      color: Colors.black54,
                      fontSize: 18,
                    ),
                    space: 35,
                  ),
                ),
                CustomDropdown(
                  shadow: true,
                  onChanged: (value) {},
                  items: [
                    DropdownMenuItem(
                      child: Text('Nhà hàng Thùy Trang - Huế'),
                    ),
                    DropdownMenuItem(
                      child: Text('Nhà hàng Rio- ĐN'),
                    ),
                    DropdownMenuItem(
                      child: Text('Nhà hàng Thắng Lợi - Phú Bài'),
                    ),
                  ],
                  hint: 'Chọn nhà hàng',
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
            color: Colors.black87,
            child: Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SortButton(
                      onChanged: (v) {},
                    ),
                    DropdownButtonHideUnderline(
                        child: DropdownButton(
                          hint: Text("TOP 5", style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold)),
                          isExpanded: false,
                          icon: Icon(Icons.keyboard_arrow_down, color: Colors.white, size: 30,),
                          onChanged: (value) {},
                          items: [
                            DropdownMenuItem(child: Text("TOP 5"),
                              value: 5,
                            ),
                            DropdownMenuItem(child: Text("TOP 10"),
                              value: 10,
                            ),
                            DropdownMenuItem(child: Text("TOP 15"),
                              value: 5,
                            ),
                          ],
                          isDense: true,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                        )),
                  ],
                ),
                SizedBox(
                  height: 20,
                ),
                TopFoodItem(
                  isFirst: true,
                  leading: "1",
                  title: "TÔM HÙM A",
                  trailing: "200",
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RoundedExpansionCard(
                        title: "Thống Kê Theo Tuần",
                        children: [
                          Row(
                            children: [
                              Flexible(
                                  flex: 1,
                                  child: Column(
                                    children: [
                                      DotWidget(
                                        color: Colors.greenAccent,
                                        text: "Hôm Nay",
                                      ),
                                      DotWidget(
                                        color: Colors.grey,
                                        text: "Hôm Qua",
                                      ),
                                    ],
                                  )
                              ),
                              Flexible(
                                flex: 1,
                                child: BarChartWeekly(
                                  [
                                    Bar(x: 0, y1: 200, y2: 100),
                                  ],
                                  height: 80,
                                  showDot: false,
                                  showGrid: true,
                                  gridStep: 30,
                                  y1Color: Colors.greenAccent,
                                  unit: "Lượt",
                                  columnWidth: 8,
                                ),)
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                TopFoodItem(
                  isFirst: false,
                  leading: "2",
                  title: "TÔM HÙM B",
                  trailing: "180",
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RoundedExpansionCard(
                        title: "Thống Kê Theo Ngày",
                        children: [
                          Row(
                            children: [
                              Flexible(
                                  flex: 1,
                                  child: Column(
                                    children: [
                                      DotWidget(
                                        color: Colors.greenAccent,
                                        text: "Hôm Nay",
                                      ),
                                      DotWidget(
                                        color: Colors.grey,
                                        text: "Hôm Qua",
                                      ),
                                    ],
                                  )
                              ),
                              Flexible(
                                flex: 1,
                                child: BarChartWeekly(
                                  [
                                    Bar(x: 0, y1: 200, y2: 100),
                                  ],
                                  height: 80,
                                  showDot: false,
                                  showGrid: true,
                                  gridStep: 30,
                                  y1Color: Colors.greenAccent,
                                  unit: "Lượt",
                                  columnWidth: 8,
                                ),)
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                TopFoodItem(
                  isFirst: false,
                  leading: "3",
                  title: "TÔM HÙM C",
                  trailing: "160",
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RoundedExpansionCard(
                        title: "Thống Kê Theo Ngày",
                        children: [
                          Row(
                            children: [
                              Flexible(
                                  flex: 1,
                                  child: Column(
                                    children: [
                                      DotWidget(
                                        color: Colors.greenAccent,
                                        text: "Hôm Nay",
                                      ),
                                      DotWidget(
                                        color: Colors.grey,
                                        text: "Hôm Qua",
                                      ),
                                    ],
                                  )
                              ),
                              Flexible(
                                flex: 1,
                                child: BarChartWeekly(
                                  [
                                    Bar(x: 0, y1: 200, y2: 100),
                                  ],
                                  height: 80,
                                  showDot: false,
                                  showGrid: true,
                                  gridStep: 30,
                                  y1Color: Colors.greenAccent,
                                  unit: "Lượt",
                                  columnWidth: 8,
                                ),)
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                TopFoodItem(
                  isFirst: false,
                  leading: "4",
                  title: "TÔM HÙM D",
                  trailing: "140",
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RoundedExpansionCard(
                        title: "Thống Kê Theo Ngày",
                        children: [
                          Row(
                            children: [
                              Flexible(
                                  flex: 1,
                                  child: Column(
                                    children: [
                                      DotWidget(
                                        color: Colors.greenAccent,
                                        text: "Hôm Nay",
                                      ),
                                      DotWidget(
                                        color: Colors.grey,
                                        text: "Hôm Qua",
                                      ),
                                    ],
                                  )
                              ),
                              Flexible(
                                flex: 1,
                                child: BarChartWeekly(
                                  [
                                    Bar(x: 0, y1: 200, y2: 100),
                                  ],
                                  height: 80,
                                  showDot: false,
                                  showGrid: true,
                                  gridStep: 30,
                                  y1Color: Colors.greenAccent,
                                  unit: "Lượt",
                                  columnWidth: 8,
                                ),)
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                TopFoodItem(
                  isFirst: false,
                  leading: "5",
                  title: "TÔM HÙM E",
                  trailing: "140",
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RoundedExpansionCard(
                        title: "Thống Kê Theo Ngày",
                        children: [
                          Row(
                            children: [
                              Flexible(
                                  flex: 1,
                                  child: Column(
                                    children: [
                                      DotWidget(
                                        color: Colors.greenAccent,
                                        text: "Hôm Nay",
                                      ),
                                      DotWidget(
                                        color: Colors.grey,
                                        text: "Hôm Qua",
                                      ),
                                    ],
                                  )
                              ),
                              Flexible(
                                flex: 1,
                                child: BarChartWeekly(
                                  [
                                    Bar(x: 0, y1: 200, y2: 100),
                                  ],
                                  height: 80,
                                  showDot: false,
                                  showGrid: true,
                                  gridStep: 30,
                                  y1Color: Colors.greenAccent,
                                  unit: "Lượt",
                                  columnWidth: 8,
                                ),)
                            ],
                          )
                        ],
                      ),
                    ),
                  ],
                ),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ButtonTheme(
                      child: RaisedButton(
                        onPressed: () {
                          print('OK');
                        },
                        color: Colors.green,
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(30.0)),
                        child: Container(
                          width: 250,
                          child: Center(
                            child: Text(
                              'XUẤT BÁO CÁO',
                              style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  _listWeek(int weeks) {
    final _list = <CustomSelectButton>[]..add(
      CustomSelectButton(
        child: Text('Tuần này'),
        value: 0,
      ),
    );
    var currentDate = DateTime.now();
    for (int i = 0; i < weeks; i++) {
      final endDate = currentDate.subtract(
        Duration(days: currentDate.weekday),
      );
      final startDate = endDate.subtract(
        Duration(days: DateTime.daysPerWeek),
      );
      currentDate = endDate;
      if (startDate.month == endDate.month) {
        _list.add(
          CustomSelectButton(
            child: Text(
                '${DateFormat('d').format(startDate)}-${DateFormat('d/M').format(endDate)}'),
            value: '${startDate.day}.${endDate.day}',
          ),
        );
      } else {
        _list.add(CustomSelectButton(
          child: Text(
              '${DateFormat('d/M').format(startDate)}-${DateFormat('d/M').format(endDate)}'),
          value: '${startDate.day}.${endDate.day}',
        ));
      }
    }

    return _list.reversed.toList();
  }
}