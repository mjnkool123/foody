import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/presentation/admin/food/redo/redo_tab.dart';
import 'package:foody/presentation/widgets/filter.dart';
import 'package:intl/intl.dart';
import 'package:md2_tab_indicator/md2_tab_indicator.dart';

import '../../presentation.dart';

class AdminFoodTab extends StatefulWidget {
  @override
  _AdminFoodTabState createState() => _AdminFoodTabState();
}

class _AdminFoodTabState extends State<AdminFoodTab>{

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.white,
            bottom: PreferredSize (
              preferredSize: const Size.fromHeight(80),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        '${DateFormat('H:m d/M/y').format(DateTime.now())}',
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          "Thống Kê Món Ăn",
                          style: TextStyle(fontSize: 18),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20),
                        child: Container(
                            width: 40.0,
                            height: 40.0,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle, color: Colors.black),
                            child: IconButton(
                              icon: Icon(
                                Icons.filter_alt,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                      return Filter();
                                    });
                              },
                            )),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(2.0),
                    child: TabBar(
                      indicator: MD2Indicator(
                        indicatorSize: MD2IndicatorSize.normal,
                        indicatorHeight: 5,
                        indicatorColor: Colors.red,
                      ),
                      labelPadding: EdgeInsets.all(2.0),
                      indicatorSize: TabBarIndicatorSize.label,
                      labelColor: Colors.red,
                      unselectedLabelColor: Colors.grey,
                      tabs: [
                        Tab(
                            child: Text("LƯỢT ĐẶT",
                                style: TextStyle(fontWeight: FontWeight.bold))),
                        Tab(
                            child: Text("DOANH THU",
                                style: TextStyle(fontWeight: FontWeight.bold))),
                        Tab(
                            child: Text("LÀM LẠI",
                                style: TextStyle(fontWeight: FontWeight.bold))),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              BookingTab(),
              RevenueTab(),
              RedoTab(),
            ],
          ),
        ));
  }
}
