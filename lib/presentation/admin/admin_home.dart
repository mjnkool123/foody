import 'package:flutter/material.dart';
import 'package:md2_tab_indicator/md2_tab_indicator.dart';
import '../presentation.dart';

class AdminHomeScreen extends StatefulWidget {
  @override
  _AdminPageSate createState() => _AdminPageSate();
}

class _AdminPageSate extends State<AdminHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 4,
        child: Scaffold(
          body: NestedScrollView (
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  backgroundColor: Colors.green,
                  floating: true,
                  snap: true,
                  bottom: PreferredSize(
                    preferredSize: const Size.fromHeight(100),
                    child: Stack(
                      children: <Widget>[
                        Container(
                          color: Colors.black87,
                          height: 150,
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                              margin: EdgeInsets.only(bottom: 15),
                              child: TabBar(
                                indicator: MD2Indicator(
                                  indicatorSize: MD2IndicatorSize.normal,
                                  indicatorHeight: 5,
                                  indicatorColor: Colors.yellow,
                                ),
                                labelPadding: EdgeInsets.all(2.0),
                                indicatorWeight: 2,
                                indicatorSize: TabBarIndicatorSize.label,
                                labelColor: Colors.yellow[400],
                                unselectedLabelColor: Colors.grey,
                                tabs: [
                                  Tab(
                                      child: Text("KHÁCH HÀNG",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold))),
                                  Tab(
                                      child: Text("MÓN ĂN",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold))),
                                  Tab(
                                      child: Text("DOANH THU",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold))),
                                  Tab(
                                      child: Text("CƠ SỞ",
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold))),
                                ],
                              ),
                            ),
                          ),
                        ),
                        Container(
                          color: Colors.green,
                          height: 50,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: IconButton(
                                    icon: Icon(
                                      Icons.arrow_back_ios,
                                      color: Colors.white,
                                    ),
                                    onPressed: null),
                              ),
                              IconButton(
                                  icon: Icon(
                                    Icons.menu,
                                    color: Colors.white,
                                    size: 34,
                                  ),
                                  onPressed: null),
                            ],
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          heightFactor: 1.0,
                          child: Card(
                            color: Colors.transparent,
                            elevation: 0.0,
                            child: Container(
                              alignment: Alignment.bottomCenter,
                              width: 80.0,
                              height: 80.0,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage(
                                          'assets/images/avatar_admin.jpeg'),
                                      fit: BoxFit.fill),
                                  border: Border.all(
                                      width: 4.0, color: Colors.white)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ];
            },
            body: TabBarView(
              children: <Widget>[
                AdminCustomerTab(),
                AdminFoodTab(),
                AdminRevenueTab(),
                AdminPlaceTab(),
              ],
            ),
          ),
        ));
  }
}
