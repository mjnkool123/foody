import 'package:flutter/material.dart';

class CustomDropdown extends StatefulWidget {
  final String hint;
  final List<DropdownMenuItem> items;
  final Function(dynamic) onChanged;
  final bool shadow;
  final value;
  final EdgeInsets padding;

  const CustomDropdown({Key key,
    @required this.hint,
    @required this.items,
    @required this.onChanged,
    this.shadow = false, this.value, this.padding})
      : super(key: key);

  @override
  _CustomDropdownState createState() => _CustomDropdownState();
}

class _CustomDropdownState extends State<CustomDropdown> {
  var _currentCode;

  @override
  void initState() {
    if (widget.value != null) {
      _currentCode = widget.value;
    }
      super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: widget.padding ?? const EdgeInsets.only(left: 10, right: 10, bottom: 10),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(25),
          boxShadow: widget.shadow
              ? [
            BoxShadow(
                color: Colors.grey,
                offset: Offset(1, 3),
                spreadRadius: 1,
                blurRadius: 6)
          ]
              : [],
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            icon: Icon(Icons.keyboard_arrow_down),
            items: widget.items,
            value: _currentCode,
            elevation: 0,
            dropdownColor: Colors.white,
            onTap: () => FocusScope.of(context).unfocus(),
            onChanged: (value) {
              widget.onChanged(value);
              setState(() {
                _currentCode = value;
              });
            },
            hint: Text(widget.hint ?? 'null'),
            isExpanded: true,
          ),
        ),
      ),
    );
  }
}
