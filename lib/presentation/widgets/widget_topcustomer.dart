import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TopCustomerItem extends StatelessWidget {


  TopCustomerItem({
    this.isFirst,
    this.leading,
    this.title,
    this.trailing,
    this.trailingSub,
    this.description});


  final bool isFirst;
  final String leading;
  final String title;
  final String description;
  final String trailing;
  final String trailingSub;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: FittedBox(
        child: Material(
            elevation: 8.0,
            borderRadius: BorderRadius.circular(18.0),
            shadowColor: Color(0x802196F3),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  height: 60,
                  width: 60,
                  decoration: BoxDecoration(
                    color: isFirst ? Colors.red : Colors.black54,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.horizontal(
                      left: Radius.circular(15.0),
                    ),
                  ),
                  child: Center(
                      child: Text(leading ?? "0",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: isFirst ? 50 : 35))),
                ),
                Container(
                  height: 60,
                  width: 230,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(title ?? "null", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),
                            Text(description ?? "null", style: TextStyle(color: Colors.grey),)
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 60,
                  width: 60,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(trailing ?? "", style: TextStyle(fontSize: 20,color: isFirst ? Colors.red : Colors.black87)),
                      Text(trailingSub ?? "Triệu", style: TextStyle(color: Colors.grey))
                    ],
                  ),
                ),
              ],)
        ),
      ),
    );
  }
}
