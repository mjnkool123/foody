import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';

enum ContactInfoStatus{
  CAME,
  COMING,
  CANCEL,
}

class ContactInfo extends StatelessWidget{
  final String name;
  final String phoneNumber;
  final String code;
  final String setTime;
  final ContactInfoStatus status;

  const ContactInfo({Key key,
    this.name,
    this.phoneNumber,
    this.code,
    this.setTime,
    this.status}) : super(key: key);

  _check() {
    Color color;
    switch (status) {
      case ContactInfoStatus.CAME:
        color = Colors.greenAccent;
        break;
      case ContactInfoStatus.COMING:
        color = Colors.yellow;
        break;
      case ContactInfoStatus.CANCEL:
        color = Colors.red;
        break;
      default:
        color = Colors.cyan;
        break;
    }
    return color;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      child: FittedBox(
        child: Material(
            elevation: 4.0,
            borderRadius: BorderRadius.circular(100.0),
            shadowColor: Color(0x802196F3),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  width: SizeConfig.blockSizeHorizontal * 20,
                  height: SizeConfig.blockSizeHorizontal * 20,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100.0),
                    child: Container(
                      color: _check(),
                      child: Icon(Icons.person, size: 50, color: Colors.white,),
                    ),
                  ),
                ),
                Container(
                  width: SizeConfig.blockSizeHorizontal * 30,
                  child: Column(
                    children: [
                      Text( code ?? "",style: TextStyle(color: Colors.grey, fontSize: 10)),
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text( name ?? ""),
                            Text( phoneNumber ?? "")
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  width: SizeConfig.blockSizeHorizontal * 15,
                  height: SizeConfig.blockSizeHorizontal * 20,
                  decoration: BoxDecoration(
                    color: _check(),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.horizontal(
                      right: Radius.circular(20.0),
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.alarm, color: Colors.white),
                      Text(setTime ?? "", style: TextStyle(color: Colors.white))
                    ],
                  ),
                ),
              ],)
        ),
      ),
    );
  }

}