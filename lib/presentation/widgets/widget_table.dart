import 'dart:math';
import 'package:flutter/material.dart';
import 'package:foody/configs/config.dart';

enum TableStatus {
  EMPTY,
  BOOKED,
  CAME,
}

class TableWidget extends StatelessWidget {
  final int chairsSlot;
  final TableStatus status;
  final int booked;
  final bool showText;
  final String tableName;

  const TableWidget(
      {Key key,
        @required this.chairsSlot,
        @required this.status,
        this.booked = 0,
        this.tableName,
        this.showText = false})
      : super(key: key);

  _check() {
    Color color;
    switch (status) {
      case TableStatus.EMPTY:
        color = Colors.green;
        break;
      case TableStatus.BOOKED:
        color = Colors.yellow;
        break;
      case TableStatus.CAME:
        color = Colors.red;
        break;
      default:
        color = Colors.cyan;
        break;
    }
    return color;
  }

  List<Align> _stacks() {
    Color color;
    final list = <Align>[];
    list.add(Align(
      alignment: Alignment(0, 0),
      child: Container(
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          color: _check(),
          shape: BoxShape.circle,
        ),
        child: showText ? Center(
          child: Text(
            '$booked/$chairsSlot',
            style: TextStyle(
              fontSize: 13,
              fontWeight: FontWeight.bold,
            ),
          ),
        ) : null
      ),
    ));
    for (var i = 0; i <= chairsSlot; i++) {
      if (i <= booked) {
        color = Colors.yellow;
      } else {
        color = _check();
      }
      var degree = 360 / chairsSlot * ( i + 0.5 );
      var radian = degree * (pi / 180);
      list.add(
        Align(
          alignment: Alignment(sin(radian), cos(radian)),
          child: Transform.rotate(
            angle: -radian,
            child: Image.asset(
              'assets/images/chair.png',
              height: 14,
              color: color,
            ),
          ),
        ),
      );
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Column(
      children: [
        SizedBox(
          height: 70,
          width: 70,
          child: Stack(
            children: _stacks(),
          ),
        ),
        Text(tableName ?? '' , style: TextStyle(color: Colors.white, fontSize: SizeConfig.safeBlockHorizontal * 4 ),),
      ],
    );
  }
}
