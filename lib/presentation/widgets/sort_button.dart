import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

enum SortType {high, low}

class SortButton extends StatefulWidget {
  final Function(SortType) onChanged;

  const SortButton({Key key, this.onChanged}) : super(key: key);
  @override
 _SortButtonState createState() => _SortButtonState();
}

class _SortButtonState extends State<SortButton> {
  SortType currentSort;
  @override
  void initState() {
    currentSort = SortType.high;
    widget.onChanged(currentSort);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              currentSort = SortType.high;
            });
            widget.onChanged(currentSort);
          },
          child: Container(
            child: Container(
              height: 40,
              width: 130,
              decoration: BoxDecoration(
                color: currentSort == SortType.high
                    ? Colors.greenAccent
                    : Colors.white,
                borderRadius: BorderRadius.vertical(top: Radius.circular(10)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.sort, color: Colors.black54),
                  Text('Cao nhất'),
                ],
              ),
            ),
          ),
        ),
        GestureDetector(
          onTap: () {
            setState(() {
              currentSort = SortType.low;
            });
            widget.onChanged(currentSort);
          },
          child: Container(
            child: Container(
              height: 40,
              width: 130,
              decoration: BoxDecoration(
                color: currentSort == SortType.low
                    ? Colors.greenAccent
                    : Colors.white,
                borderRadius: BorderRadius.vertical(bottom: Radius.circular(10)),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Icon(Icons.sort, color: Colors.black54),
                  Text('Thấp nhất'),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}