import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Filter extends StatefulWidget {
  @override
  _FilterState createState() => _FilterState();
}

class _FilterState extends State<Filter> {

  final TextEditingController _txtController1 = new TextEditingController();
  final TextEditingController _txtController2 = new TextEditingController();
  final TextEditingController _txtController3 = new TextEditingController();
  String txtFied1 = "Xem Theo Ngày";
  String txtFied2 = "Hôm Qua";
  String txtFied3 = "Ngày Này Năm Trước";

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Scaffold(
        backgroundColor: Colors.black54,
        appBar: AppBar(
          leading: IconButton(
            onPressed: (){
              Navigator.pop(context);
            },
            icon: Icon(Icons.close, size: 30,),
          ),
          backgroundColor: Colors.black54,
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Text("CHỌN KIỂU XEM", style: TextStyle(color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold),)),
              Container(
                width: 350,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Material(
                    elevation: 3.0,
                    shadowColor: Colors.blue,
                    borderRadius: BorderRadius.circular(25.7),
                    child: TextField(
                      readOnly: true,
                      controller: _txtController1,
                      autofocus: false,
                      style:
                      TextStyle(fontSize: 17.0, color: Colors.black),
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white,
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        suffixIcon: Container(
                          width: 320,
                          child: DropdownButton<String>(
                            underline: Container(
                              height: 2,
                              color: Colors.transparent,
                            ),
                            isExpanded: true,
                            value: txtFied1,
                            onChanged: (String newValue) {
                              setState(() {
                                txtFied1 = newValue;
                              });
                            },
                            items: <String>['Xem Theo Ngày', 'Xem Theo Tuần', 'Xem Theo Tháng', 'Xem Theo Quý', 'Xem Theo Năm']
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                            icon: const Icon(Icons.arrow_drop_down),

                          ),
                        ),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Colors.white),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                  padding: EdgeInsets.all(10.0),
                  child: Text("SO SÁNH VỚI", style: TextStyle(color: Colors.white, fontSize: 25, fontWeight: FontWeight.bold),)),
              Container(
                width: 350,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Material(
                        elevation: 3.0,
                        shadowColor: Colors.blue,
                        borderRadius: BorderRadius.circular(25.7),
                        child: TextField(
                          readOnly: true,
                          controller: _txtController2,
                          autofocus: false,
                          style:
                          TextStyle(fontSize: 17.0, color: Colors.black),
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            contentPadding: const EdgeInsets.only(
                                left: 14.0, bottom: 8.0, top: 8.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            suffixIcon: Container(
                              width: 320,
                              child: DropdownButton<String>(
                                underline: Container(
                                  height: 2,
                                  color: Colors.transparent,
                                ),
                                isExpanded: true,
                                value: txtFied2,
                                onChanged: (String newValue) {
                                  setState(() {
                                    txtFied2 = newValue;
                                  });
                                },
                                items: <String>['Hôm Qua', 'Hôm Trước', 'Tháng Trước', 'Quý Trước']
                                    .map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                icon: const Icon(Icons.arrow_drop_down),

                              ),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Material(
                        elevation: 3.0,
                        shadowColor: Colors.blue,
                        borderRadius: BorderRadius.circular(25.7),
                        child: TextField(
                          readOnly: true,
                          controller: _txtController3,
                          autofocus: false,
                          style:
                          TextStyle(fontSize: 17.0, color: Colors.black),
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            contentPadding: const EdgeInsets.only(
                                left: 14.0, bottom: 8.0, top: 8.0),
                            focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                            suffixIcon: Container(
                              width: 320,
                              child: DropdownButton<String>(
                                underline: Container(
                                  height: 2,
                                  color: Colors.transparent,
                                ),
                                isExpanded: true,
                                value: txtFied3,
                                onChanged: (String newValue) {
                                  setState(() {
                                    txtFied3 = newValue;
                                  });
                                },
                                items: <String>['Ngày Này Năm Trước', 'Tháng Này Năm Trước', 'Quý Này Năm Trước',]
                                    .map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                icon: const Icon(Icons.arrow_drop_down),

                              ),
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                              borderRadius: BorderRadius.circular(25.7),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(bottom: 30.0),
                child: ButtonTheme(
                  minWidth: 350,
                  child:  RaisedButton(onPressed: () {
                    Navigator.pop(context);
                  },
                    color: Colors.yellow[600],
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0)),
                    child: Text("ÁP DỤNG",
                        style: new TextStyle(
                            color: Colors.white,
                            fontSize: 16.0,
                            fontWeight: FontWeight.bold)
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}