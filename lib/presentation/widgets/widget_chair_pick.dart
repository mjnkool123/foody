import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChairPick extends StatefulWidget {
  final int chairsSlot;

  const ChairPick({Key key, this.chairsSlot}) : super(key: key);

  @override
  _ChairPickState createState() => _ChairPickState();
}

class _ChairPickState extends State<ChairPick> {
  List<bool> _chairSelect;
  bool selectAll;

  @override
  void initState() {
    _chairSelect = List.generate(widget.chairsSlot, (index) => false);
    selectAll = false;
    super.initState();
  }

  List<Widget> _stacks() {
    final list = <Widget>[];
    list.add(Align(
      alignment: Alignment(0, 0),
      child: Container(
        height: 140,
        width: 140,
        decoration: BoxDecoration(
          color: Colors.green,
          shape: BoxShape.circle,
        ),
      ),
    ));
    for (var i = 0; i < widget.chairsSlot; i++) {
      var degree = 360 / widget.chairsSlot * (i + 0.5);
      var radian = degree * (pi / 180);
      list.add(
        GestureDetector(
          onTap: () {
            setState(() {
              _chairSelect[i] = !_chairSelect[i];
            });
          },
          child: Align(
            alignment: Alignment(sin(radian), cos(radian)),
            child: Transform.rotate(
              angle: -radian,
              child: Container(
                padding: EdgeInsets.all(16),
                decoration: BoxDecoration(
                  color: _chairSelect[i] ? Colors.green : Colors.transparent,
                  shape: BoxShape.circle,
                ),
                child: Image.asset(
                  'assets/images/chair.png',
                  height: 60,
                  color: _chairSelect[i] ? Colors.white : Colors.green,
                ),
              ),
            ),
          ),
        ),
      );
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 340,
          width: 340,
          child: Stack(
            children: _stacks(),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text("Chọn Tất Cả Các Ghế", style: TextStyle(color: Colors.white),),
            CupertinoSwitch(value: selectAll,   onChanged: (value) {
              setState(() {
                selectAll = !selectAll;
                if (value) {
                  _chairSelect =
                      List.generate(widget.chairsSlot, (index) => true);
                } else {
                  _chairSelect =
                      List.generate(widget.chairsSlot, (index) => false);
                }
              });
            })
          ],
        ),
      ],
    );
  }
}
