import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NumericTableChart extends StatelessWidget {
  final bool isGrow;
  final String title;
  final String desc;
  final Icon icon;

  const NumericTableChart({
    Key key,
    this.isGrow = true,
    this.title,
    this.desc,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          icon ?? Icon(
            isGrow ? Icons.arrow_upward_rounded : Icons.arrow_downward,
            color: isGrow ? Colors.greenAccent : Colors.red,
          ),
          SizedBox(width: 4),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title ?? 'null',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Colors.white
                ),
              ),
              Text(desc ?? 'null', style: TextStyle(color: Colors.white)),
            ],
          )
        ],
      ),
    );
  }
}
