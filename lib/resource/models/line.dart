import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class Line {
  final List<FlSpot> spots;
  final Color color;
  final String note;

  Line({this.note,this.spots, this.color});

  @override
  String toString() {
    return 'Line{spots: $spots, color: $color, note: $note}';
  }
}