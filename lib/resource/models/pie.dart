import 'dart:ui';

class Pie {
  final String title;
  final String note;
  final Color color;
  final double value;

  const Pie({this.title, this.color, this.value, this.note});

  @override
  String toString() {
    return 'Pie{title: $title, note: $note, color: $color, value: $value}';
  }
}